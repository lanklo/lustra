<?php

/**
 * This is the model class for table "candidates".
 *
 * The followings are the available columns in table 'candidates':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $modarate
 * @property string $firstname
 * @property string $lastname
 * @property string $secondname
 * @property integer $category_id
 * @property integer $party_id
 * @property string $photo
 * @property string $description
 * @property integer $author_id
 * @property string $text
 */
class Candidates extends CActiveRecord
{
        const ACTIVE = 1;
        const MODARATE = 0;
        protected $_path = "uploaded/";
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'candidates';
	}

        public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                )
            );
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstname, lastname, description', 'required'),
                        array('text', 'safe'),
			array('category_id, party_id, author_id', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname, secondname, photo, description', 'length', 'max'=>150),
                        array('firstname, lastname, secondname, description', 'normalise'),
			array('description', 'length', 'max'=>500),
                        array('photo', 'file',
                            'allowEmpty' => true,
                            'types' => 'jpg, jpeg, gif, png',
                            'maxSize' => 1024 * 1024 * 1
                        ),
                        array('modarate', 'default', 'setOnEmpty' => true, 'value' => '0'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, create_time, update_time, modarate, firstname, lastname, secondname, category_id, party_id, photo, description, author_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'category' => array(self::BELONGS_TO, 'Categories', 'category_id'),
                    'party' => array(self::BELONGS_TO, 'Parties', 'party_id'),
                    'author' => array(self::BELONGS_TO, 'users', 'author_id'),
                    'lustrations' => array(self::HAS_MANY, 'Lustrations', 'candidate_id'),
                    'lustrationCount' => array(self::STAT, 'Lustrations', 'candidate_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Дата створення',
			'update_time' => 'Дата оновлення',
			'modarate' => 'Статус',
			'firstname' => "Iм'я",
			'lastname' => 'Прізвище',
			'secondname' => 'По батькові',
			'category_id' => 'Категорiя',
			'party_id' => 'Партия',
			'photo' => 'Фото',
			'description' => 'Опис',
			'author_id' => 'Користувач',
                        'text' => 'Повний текст'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($searchString = false, $pageSize = 6, $order = false)
	{
            $criteria=new CDbCriteria;

            $this->addSearchStringCriteria($searchString, $criteria);

            $criteria->compare('id',$this->id);
            $criteria->compare('modarate',$this->modarate);
            $criteria->compare('firstname',$this->firstname,true);
            $criteria->compare('lastname',$this->lastname,true);
            $criteria->compare('secondname',$this->secondname,true);
            $criteria->compare('category_id',$this->category_id);
            $criteria->compare('party_id',$this->party_id);
            $criteria->compare('author_id',$this->author_id);
//            $criteria->select = '*, (SELECT COUNT(*) FROM lustrations l WHERE l.candidate_id = t.id) as countL';
//            $criteria->order = 'countL desc';
            if($order && $order == 'rand'){
                if(!isset(Yii::app()->request->cookies['rand']) || !isset($_GET['Candidates_page'])){
                    Yii::app()->request->cookies['rand'] = new CHttpCookie('rand', rand(0, 100));
                }

                $criteria->order = 'rand(' . Yii::app()->request->cookies['rand'] . ')';
            }

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination' => array(
                    'pageSize' => $pageSize
                ),
            ));
	}

    private function addSearchStringCriteria($searchString, $criteria) {
        $searchString = preg_replace('/\s+/', ' ', $searchString);
        if (strlen($searchString) > 3) {
            $newCriteria = new CDbCriteria;
            $wordsArray = explode(" ", $searchString);
            foreach ($wordsArray as $word) {
                $newCriteria->mergeWith(array(
                    'condition' => 'CONCAT(`firstname`,`secondname`,`lastname`) LIKE :match',
                    'params'    => array(':match' => "%$word%")
                ), 'OR');
            }
            $criteria->mergeWith($newCriteria, 'AND');
        }
        return $criteria;
    }

        public function getCandidateLustrations()
        {
            return count($this->lustrations);
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Candidates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function scopes()
        {
            return array(
                'published' => array(
                    'condition' => 'modarate = 1',
                ),
            );
        }

        public function lastAdded($limit = 6)
        {
            $this->getDbCriteria()->mergeWith(array(
                'order' => 'id DESC',
                'condition' => 'modarate = 1',
                'limit' => $limit,
            ));
            return $this;
        }

        public function normalise($attribute, $params)
        {
            $this->$attribute = htmlspecialchars(trim($this->$attribute));
        }

        public function beforeSave()
        {
            if(parent::beforeSave()) {
                $this->_deleteImage();
                $this->_uploadFile('photo', 350);

                return true;
            } else
                return false;
        }

        protected function beforeDelete()
        {
            if(!parent::beforeDelete())
                return false;

            $this->deleteFile('photo');
            return true;
        }

        public function approve()
        {
            $this->modarate = self::ACTIVE;
            $this->update(array('modarate'));
        }

        public function remove()
        {
            $this->modarate = self::MODARATE;
            $this->update(array('modarate'));
        }

        public function getFullname()
        {
             $name = $this->lastname . ' ' . $this->firstname;
             if($this->secondname) $name .= ' ' . $this->secondname;
             return $name;
        }

        public function getCategoryName()
        {
            return $this->category?$this->category->title:'';
        }

        public function getPartyName()
        {
            return $this->party->title;
        }

        public function getPunishment()
        {
            $max = 0;
            $index = 0;
            foreach($this->methodStatistic as $k => $item){
                if($item->persentage > $max){
                    $max = $item->persentage;
                    $index = $k;
                }
            }
            return $this->methodStatistic[$index]->title;
        }

        public function getMethodStatistic()
        {
            $methods = Methods::model()->findAll();
            
            $lustrations = Lustrations::model()->persentageStatistic($this->id);
            
            foreach($methods as $item){
                if(isset($lustrations[$item->id]))
                    $item->persentage = round(100 * $lustrations[$item->id] / $this->lustrationCount);
                else
                    $item->persentage = 0;
            }
            return $methods;
        }

        public function getLink()
        {
            return 'link';
        }

        public function getCount()
        {
            return $this->count('modarate = :modarate', array(':modarate' => 1));
        }
        
        public function getLustrated()
        {
            if(Yii::app()->user->isGuest) return false;
            
            return Lustrations::model()->exists('user_id=:user_id AND candidate_id=:candidate_id', array(
                ':user_id' => Yii::app()->user->id,
                ':candidate_id' => $this->id,
            ));
        }

        public function findCandidate()
        {
            if(!$this->firstname || !$this->lastname)
                return false;
            
            $c = new CDbCriteria;
            $c->condition = 'firstname LIKE :firstname AND lastname LIKE :lastname';
            $c->addCondition('modarate = 1');
            $c->params = array(':firstname' => $this->firstname, ':lastname' => $this->lastname);
            if($this->secondname){
//                $c->addSearchCondition('secondname', $this->secondname);
                $c->compare('secondname', array($this->secondname, ''));
            }
            $candidates = $this->findAll($c);
            
            if(count($candidates) > 0)
                return $candidates;
            
             return false;
        }

        public function lustrate($candidateId, $methodId) {
            // Check is user able to lustrate
            if (Yii::app()->user->isGuest) {
                throw new Exception("Ви повинні бути авторизовані, щоб мати можливість люструвати");
            }
            $userId = Yii::app()->user->id;

            // Check is user already lustrated this candidate before
            $model = new Lustrations();
            $model->user_id = $userId;
            $model->candidate_id = $candidateId;
            if ($model->search()->totalItemCount > 0) {
                throw new Exception("Ви вже люстрували цього кандидата");
            }

            // All fine, we can lustrate
            $model->method_id = $methodId;

            return $model->save();
        }

        public function deleteFile($attr)
        {
            $this->_deleteFile($attr, true);
        }

        public function getImageUrl()
        {
            return $this->_getBaseImagePath() . 't/' . $this->photo;
        }

        private function _getBaseImagePath()
        {
            return Yii::app()->request->baseUrl . '/' . $this->_path;
        }

        private function _getImagePath()
        {
            return YiiBase::getPathOfAlias("webroot").DIRECTORY_SEPARATOR.$this->_path;
        }

        protected function _deleteFile($attr, $thumb = false)
        {
            $documentPath = $this->_getImagePath(). DIRECTORY_SEPARATOR . $this->$attr;

            if(is_file($documentPath)) unlink($documentPath);

            if($thumb){
                $documentPath = $this->_getImagePath(). DIRECTORY_SEPARATOR . 't' . DIRECTORY_SEPARATOR . $this->$attr;
                if(is_file($documentPath)) unlink($documentPath);
            }

            $this->$attr = null;
        }

        protected function _uploadFile($attr, $width = null, $height = null)
        {
            if(($this->scenario=='front' || $this->scenario=='insert' || $this->scenario=='update') &&
                ($file = EUploadedImage::getInstance($this, $attr)) && $file->name){
                $this->deleteFile($attr);
                $this->$attr = rand() . time() . preg_replace('/(^.*)(\.)/', '$2', $file->name);

                $folder = $this->_getImagePath();
                if (!file_exists($folder))
                    mkdir($folder, 0, true);

                $file->saveAs($folder . DIRECTORY_SEPARATOR . $this->$attr);

                $file->reset();
//                if($width > $height)
                    $file->maxWidth = $width;
//                else
//                    $file->maxHeight = $height;

                if (!file_exists($folder .DIRECTORY_SEPARATOR . 't'))
                    mkdir($folder .DIRECTORY_SEPARATOR . 't', 0, true);

                $file->saveAs($folder .DIRECTORY_SEPARATOR . 't' . DIRECTORY_SEPARATOR . $this->$attr);
            }
        }

        protected function _deleteImage()
        {
            if(isset($_POST[get_class($this)]['delete']))
                foreach($_POST[get_class($this)]['delete'] as $attr => $value){
                    if ($value) $this->deleteFile($attr);
                }
        }
}
