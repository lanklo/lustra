<?php

/**
 * This is the model class for table "lustrations".
 *
 * The followings are the available columns in table 'lustrations':
 * @property integer $id
 * @property integer $create_time
 * @property integer $candidate_id
 * @property integer $user_id
 * @property integer $method_id
 */
class Lustrations extends CActiveRecord
{
        public $count;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lustrations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('candidate_id, user_id, method_id', 'required'),
			array('candidate_id, user_id, method_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, create_time, candidate_id, user_id, method_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'candidate' => array(self::BELONGS_TO, 'Candidates', 'candidate_id'),
                    'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
                    'method' => array(self::BELONGS_TO, 'Methods', 'method_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Дата',
			'candidate_id' => 'Люстрований',
			'user_id' => 'Хто люстрував',
			'method_id' => 'Спосiб',
                        'candidate.fullname' => 'Люстрований',
                        'user.first_name' => 'Хто люстрував',
                        'method.title' => 'Спосiб',
		);
	}

        public function beforeSave()
        {
            parent::beforeSave();
            
            $this->create_time = date('Y-m-d H:i:s');
            return true;
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('candidate_id',$this->candidate_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('method_id',$this->method_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lustrations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function persentageStatistic($id)
        {
            $c = new CDbCriteria();
            $c->select = 'count(id) as count, method_id';
            $c->condition = 'candidate_id=:candidate_id';
            $c->params = array(':candidate_id' => $id);
            $c->group = 'method_id';

            return CHtml::listData($this->model()->findAll($c), 'method_id', 'count');
        }

        public function getCandidateName()
        {
            if($this->candidate)
                return $this->candidate->fullname;
        }

        public function getUserName()
        {
            if($this->user)
                return $this->user->first_name;
            return 'incognito';
        }

        public function getUserEmail()
        {
            if($this->user)
                return $this->user->email_;
        }

        public function getMethodName()
        {
            if($this->method)
                return $this->method->title;
        }

        public function xls($criteria)
        {
            $models = self::model()->findAll($criteria);
            $data = CHtml::listData($models, 'id', 'attributes');

            foreach ($models as $item){
                $data[$item->id]['candidateName'] = $item->candidateName;
                $data[$item->id]['methodName'] = $item->methodName;
                $data[$item->id]['userName'] = $item->userName;
                $data[$item->id]['userEmail'] = $item->userEmail;
            }
            return $data;
        }
}
