<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?=$this->pageTitle; ?></h1>
    </div>
</div>
<h4>Email налаштування</h4>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$emailProvider,
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        'alias',
        'value',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'htmlOptions' => array('style'=>'width: 50px', 'class' => 'button-distance'),
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'url' => 'Yii::app()->createUrl("administration/default/emailUpdate", array("code" => $data->code))',
                )
            ),
        ),
    ),
));?>
<h4>Изменить пароль администратора</h4>
    <?php $form=$this->beginWidget('AdminFormWidget', array(
        'id'=>'password-form',
        'type' => 'horizontal',
    )); ?>

    <?= $form->passwordFieldRow($user, 'password', array(
        'class'=>'span5', 
        'value' => FALSE,
        'autocomplete' => FALSE)); ?>
    <?= $form->passwordFieldRow($user, 'password_repeat', array('class'=>'span5', 'value' => '')); ?>

    <div class="controls">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>'Изменить',
        'htmlOptions' => array('name' => 'save'))
    ); ?>
    </div>
    <?php $this->endWidget(); ?>

<h4>Текст на головнiй</h4>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        'title',
        'content',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'htmlOptions' => array('style'=>'width: 50px', 'class' => 'button-distance'),
            'template' => '{update}',
            'buttons'=>array(
                'update' => array(
                    'url'=>'Yii::app()->createUrl("/administration/default/StatpageUpdate", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
));