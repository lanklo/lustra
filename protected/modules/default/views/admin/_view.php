<?php if($index && $index % 3 == 0):?><br clear="all"/><?php endif; ?>
<div class="item" style="float:left; margin-right: 30px">
    <div class="image">
        <?= CHtml::link(CHtml::image($data->imageUrl, '', array('style' => 'width:200px')),
            $this->createUrl('view', array('id' => $data->id))) ?>
    </div>
    <div class="title">
        <?= CHtml::link($data->fullName, $this->createUrl('view', array('id' => $data->id))) ?>
    </div>

    <div class="content">
        <?= $data->description ?>
    </div>
</div>