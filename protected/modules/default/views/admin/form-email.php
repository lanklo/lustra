<?php
/* @var $this AdminController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
    </div>
</div>

<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminFormWidget', array(
        'id'=>'email-form',
        'type' => 'horizontal',
    )); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'value', array('class'=>'span5')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>