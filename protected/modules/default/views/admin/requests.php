<?php
/* @var $this CategoriesController */
/* @var $model Categories */
?>
<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?=$this->pageTitle; ?></h1>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
            'url' => array('create'),
            'label' => '&nbsp;Додати кандидата',
            'icon' => 'plus white',
            'type'=>'primary',
            'encodeLabel' => false,
            'htmlOptions' => array(
                'class' => 'pull-right',
            'style' => 'margin-top:10px; margin-bottom:10px'))
        ); ?>
    </div>
</div>

<?php $buttons = array();
if(isset($mode) && $mode == 'request'){
    $buttons = array(
            'update' => array(
                'url' => 'Yii::app()->createUrl("/administration/default/update", array("mode" => "request", "id" => $data->id))'
            ),
        );
}
?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$model->search(false, 10),
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        'firstname',
        'lastname',
        'description',
        array(
            'name' => 'photo',
            'type' => 'html',
            'value' => 'CHtml::image($data->imageUrl, "", array("style" => "width:200px"))',
            'filter' => false,
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'htmlOptions' => array('style'=>'width: 50px', 'class' => 'button-distance'),
            'template' => '{update}{delete}',
            'buttons' => $buttons,
        ),
    ),
));
