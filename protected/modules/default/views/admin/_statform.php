<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?=$this->pageTitle; ?></h1>
    </div>
</div>
<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminFormWidget', array(
	'id'=>'blog-posts-form',
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
    )); ?>
    <?= $form->errorSummary($model) ?>
    <?= $form->textFieldRow($model, 'title', array('class'=>'span5')); ?>
    <?= $form->textAreaRow($model, 'content', array('class'=>'span5 mceEditor', 'rows' => 20)); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
