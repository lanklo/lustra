<?php
/* @var $this CategoriesController */
/* @var $model Categories */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
    </div>
</div>
<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminFormWidget', array(
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
    )); ?>
    <?= $form->textFieldRow($model, 'title', array('class'=>'span5')); ?>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Применить',
            'htmlOptions' => array('name' => 'apply'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
