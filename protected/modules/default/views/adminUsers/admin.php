<?php
/* @var $this CategoriesController */
/* @var $model Categories */
?>
<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'url' => array('xls'),
            'label' => '&nbsp;Статистика',
            'icon' => 'download white',
            'type'=>'primary',
            'encodeLabel' => false,
            'htmlOptions' => array(
                'class' => 'pull-right',
            'style' => 'margin-top:10px; margin-bottom:10px'))
        ); ?>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        'id',
        'create_time',
        'first_name',
        'email_',
        'social',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'htmlOptions' => array('style'=>'width: 50px', 'class' => 'button-distance'),
            'template' => '{update}{delete}',
            'deleteConfirmation'=>"js:'Усi люстрацiї користувача будуть видаленi! Продовжити?'",
        ),
    ),
));
