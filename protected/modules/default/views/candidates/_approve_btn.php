<?php if(!$model->isNewRecord): ?>
<div class="control-group ">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Зняти з публікації',
        'buttonType' => 'ajaxButton',
        'type'=>'warning',
        'loadingText' => '...',
        'size'=>'large',
        'htmlOptions' => array('style' => ($model->modarate) ? '' : 'display:none'),
        'url' => Yii::app()->createUrl('/default/candidates/approve/', array(
            'id' => $model->id,
            'remove' => 1)),
        'ajaxOptions' => array(
            'success'=>"js:function(vals){
                $('#yt0').hide();
                $('#yt1').show();
            }",
        )
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Опублікувати',
        'buttonType' => 'ajaxButton',
        'type'=>'success',
        'loadingText' => '...',
        'size'=>'large',
        'htmlOptions' => array('style' => ($model->modarate) ? 'display:none' : ''),
        'url' => Yii::app()->createUrl('/default/candidates/approve/', array('id' => $model->id)),
        'ajaxOptions' => array(
            'success'=>"js:function(vals){
                $('#yt1').hide();
                $('#yt0').show();
            }",
        )
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'url' => Yii::app()->createUrl('/default/candidates/delete', array('id' => $model->id)),
        'label' => 'Видалити',
        'htmlOptions' => array('confirm'=>'Вы уверены, что хотите удалить данный элемент?'),
        'type' => 'danger',
        'size' => 'large',
    )); ?>
</div><br/>
<?php endif; ?>