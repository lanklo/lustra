<article class="col-lg-9 col-md-12 col-sm-12 galleryBox">
    <?php if($dataProvider->party_id) :?>
        <h2>
            Всi кандидати партії "<?= $dataProvider->partyName ?>"
        </h2>
    <?php elseif ($dataProvider->category_id || $search_string): ?>
        <h2>
            Кандидати
            <?php if ($dataProvider->category_id): ?>за категорією "<?= $dataProvider->categoryName ?>"<?php endif ?>
            <?php if ($search_string): ?>за пошуковим запросом "<?= CHtml::encode($search_string) ?>"<?php endif ?>
        </h2>
    <?php else: ?>
        <h2>всі кандидати</h2>
    <?php endif; ?>
    <div class="row">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider->search($search_string, 6, 'rand'),
            'itemView'     => '_view',
            'viewData' => array('lg' => 3, 'user' => $user),
            'pager' => array(
                'header' => '',
                'prevPageLabel'=>'<',
                'nextPageLabel'=>'>',
                'class' => 'Pager'
            ),
            'ajaxUpdate'   => false,
            'template'     => '{items}<div class="clearfix"></div>{pager}'
        )); ?>
    </div>
</article>
<?php $this->renderPartial('//partials/_popup', compact('user')); ?>