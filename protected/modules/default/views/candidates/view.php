<article class="col-lg-9 col-md-12 col-sm-12 candidate-page">
    <article class="col-lg-5 col-md-6 col-sm-6" candidate="<?= $model->id ?>">
        <figure>
            <?php if(!empty($model->photo)): ?>
                <div class="round-item">
                    <a class="round-hover" href="<?= $this->createUrl('/candidates/', array('view' => $model->id)) ?>"></a>
                    <?= CHtml::image($model->imageUrl, $model->fullname, array("style"=>"width:250px;")) ?>
                <? //= CHtml::link(CHtml::image($data->imageUrl),$this->createUrl('/default/candidates/view/', array('id' => $data->id))) ?>
                </div>
            <?php else: ?>
                <?= CHtml::link('<span></span>',
                        $this->createUrl('/candidates/', array('view' => $model->id)),
                        array('style' => 'display:block; width:250px; height:180px')) ?>
            <?php endif; ?>
            <span class="count"><?= $model->lustrationCount ?></span>
        </figure>
        <?= $this->renderPartial('//partials/_likebox') ?>
    </article>
    <article class="col-lg-7 col-md-6 col-sm-6 candidate-text" candidate="<?= $model->id ?>">
        <h3><?= $model->fullname ?></h3>
        <?= CHtml::link($model->categoryName, array(
                '/default/candidates',
                'category_id' => $model->category_id),
            array(
                'class' => 'link'
            )) ?><br/>
        <div class="candidate-view">
            <?= $this->renderPartial('//partials/_lustration', array(
                'data' => $model,
                'user' => $user)) ?>
        </div>
    </article>
    <div class="clearfix"></div>
    <article class="candidate-text col-lg-10 col-md-6">
        <div class="text-block"><?= $model->description ?></div>
        <div class="text-block"><?= $model->text ?></div>
    </article>
    <div class="clearfix"></div>
    <article class="col-lg-12 col-md-3 col-sm-3">
        <h4>Коментарі</h4>
        <?= $this->renderPartial('//partials/_fbcomments') ?>
    </article>
    <?php $this->renderPartial('//partials/_popup', compact('user')); ?>
    <?php $this->renderPartial('_share_flash') ?>
</article>