<?php
/* @var $this CandidatesController */
/* @var $model Candidates */
/* @var $form CActiveForm */
?>
<section class="formBox col-lg-9 col-md-12 col-sm-12">
    <div class="container">
        <div class="row">
            <article class="col-lg-8 col-md-8 col-sm-8 contactBox2">
                <h2><?= $this->pageTitle; ?></h2>
                <?php if(Yii::app()->user->isGuest): ?>
                    <h3>Залогіньтесь від імені профілю соцмережі</h3>
                    <?php $this->widget('ext.eauth.EAuthWidget', array(
                        'action' => '/login',
                        'popup' => true
                    )); ?>
                    або
                    <? /*= CHtml::link('Інкогніто', 'javascript:void(0)', array(
                        'class' => 'btn-default btn1',
                        'data-toggle' => 'modal',
                        'data-target' => '#anonim',
                        'style' => 'margin: 0 0 0 5px'
                    )) */?>
                    <?= CHtml::link('Інкогніто', 'javascript:void(0)', array(
                        'class' => 'btn-default btn1',
                        'onclick' => 'anonimAuthorize()',
                        'style' => 'margin: 0 0 0 5px'
                    )) ?>
                <?php else: ?>
                    <?php if(Yii::app()->user->hasFlash('new-candidate')): ?>
                        <?= Yii::app()->user->getFlash('new-candidate') ?>
                        <?php if(Yii::app()->user->hasFlash('lustration-vkontakte')): ?>
                            <script>
                                vkshareSimple('<?= $this->createAbsoluteUrl('/') ?>','<?= Yii::app()->user->getFlash('lustration-vkontakte') ?>');
                            </script>
                        <?php endif ?>
                        <?php if(Yii::app()->user->hasFlash('lustration-facebook')): ?>
                            <script>
                                fbshareSimple('<?= $this->createAbsoluteUrl('/') ?>','<?= Yii::app()->user->getFlash('lustration-facebook') ?>');
                            </script>
                        <?php endif ?>
                    <?php else: ?>
                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                            'enableAjaxValidation'=>false,
                            'htmlOptions'=>array('enctype' => 'multipart/form-data'),
                        )); ?>

                        <?php if($candidates): ?>
                            <div class="holder">
                                <p>Можливо, ви мали на увазі:
                                <?php foreach ($candidates as $item): ?>
                                    <?= CHtml::link($item->fullname, $this->createUrl('/candidates/', array('view' => $item->id)), array('target' => '_blank')) ?>
                                <?php endforeach; ?>
                                </p>
                            </div>
                        <?php endif; ?>
                        <?= $form->errorSummary($model); ?>

                        <div class="holder">
                            <div class="form-div-1 clearfix">
                                <label class="firstname">
                                    <?= $form->textField($model, 'firstname', array(
                                        'placeholder' => "Iм'я кандидата*:",
                                        'data-constraints' => "@Required @JustLetters")) ?>
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid name.</span>
                                </label>
                            </div>
                            <div class="form-div-2 clearfix">
                                <label class="lastname">
                                    <?= $form->textField($model, 'lastname', array(
                                        'placeholder' => "Прізвище кандидата*:",
                                        'data-constraints' => "@Required @JustLetters")) ?>
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid name.</span>
                                </label>
                            </div>
                            <div class="form-div-3 clearfix">
                                <label class="secondname">
                                    <?= $form->textField($model, 'secondname', array(
                                        'placeholder' => "По батькові кандидата:")) ?>
                                    <span class="error-message">*This is not a valid name.</span>
                                </label>
                            </div>
                        </div>
                        <div class="holder">
                            <div class="form-div-2 clearfix" style="height:80px;overflow: hidden">
                                <label class="photo">
                                    Завантажити фото кандидата
                                    <?= $form->fileField($model, 'photo', array('class' => 'file', 'accept' => 'image/*')); ?>
                                </label>
                            </div>
                            <?php /*<div class="form-div-2 clearfix">
                                <label class="message">
                                    <?= $form->dropDownList($model, 'category_id',
                                        CHtml::listData(Categories::model()->findAll(), 'id', 'title'),
                                        array('empty' => 'Категорiя', 'class' => 'form-control')); ?>
                                </label>
                            </div>*/?>
                        </div>
                        <div class="form-div-5 clearfix">
                            <label class="message">
                                <?= $form->textArea($model, 'description', array(
                                    'placeholder' => 'Опис',
                                    'data-constraints' => '@Required @Length(min=20,max=999999)'
                                    )); ?>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*The message is too short.</span>
                            </label>
                        </div>
                        <div class="btns">
                            <input type="submit" value="Вiдправити" class="btn-default btn1"/>
                        </div>
                        <?php $this->endWidget(); ?>
                    <?php endif; ?>
                <?php endif; ?>
            </article>
        </div>
    </div>
</section>
<?php $this->renderPartial('//partials/_popup', compact('user')); ?>
<?php $this->renderPartial('//partials/_anonim', compact('user')); ?>