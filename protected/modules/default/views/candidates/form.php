<?php
/* @var $this CandidatesController */
/* @var $model Candidates */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageHeader; ?></h1>
    </div>
</div>
<?php $this->renderPartial('_approve_btn', array('model' => $model)); ?>

<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminFormWidget', array(
        'id'=>'candidates-form',
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data'),
    )); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'firstname', array('class'=>'span5')); ?>
    <?= $form->textFieldRow($model, 'lastname', array('class'=>'span5')); ?>
    <?= $form->textFieldRow($model, 'secondname', array('class'=>'span5')); ?>
    <?= $form->dropDownListRow($model, 'category_id',
            CHtml::listData(Categories::model()->findAll(), 'id', 'title'),
            array('empty' => '')); ?>

    <?= $form->dropDownListRow($model, 'party_id',
            CHtml::listData(Parties::model()->findAll(), 'id', 'title'),
            array('empty' => '')); ?>
    <?= $form->imageFieldRow($model, 'photo'); ?>
    <?= $form->textAreaRow($model, 'description', array('class'=>'span5', 'rows' => 8)); ?>
    <?= $form->textAreaRow($model, 'text', array('class'=>'span5', 'rows' => 10)); ?>
    <?= $form->textFieldRow($model, 'author_id', array('class'=>'span5')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Применить',
            'htmlOptions' => array('name' => 'apply'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>