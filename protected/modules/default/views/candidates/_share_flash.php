<?php if(Yii::app()->user->hasFlash('lustration-facebook')): ?>
    <script type="text/javascript">
        $(document).ready(function(){
            window.open('https://www.facebook.com/sharer/sharer.php?u='
                +'<?= urlencode($this->createAbsoluteUrl('/') . '/' . Yii::app()->request->pathInfo) ?>'
                +'&display=popup',
                "",
                "width=600,height=400,status=no,toolbar=no,menubar=no");
        });
    </script>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('lustration-vkontakte')): ?>
    <script type="text/javascript">
        $(document).ready(function(){
            window.open('http://vk.com/share.php?url=<?= urlencode($this->createAbsoluteUrl('/') . '/' . Yii::app()->request->pathInfo) ?>?act=share',
            '',
            '');
        });
    </script>
<?php endif; ?>