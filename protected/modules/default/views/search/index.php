<section class="content-section">

   <div class="wrapper980 search-res-block clearfix">
        <?php $this->widget('application.components.BreadCrumb', array(
                     'crumbs' => $this->breadcrumbs,
                     'delimiter' => ' &gt; ', // if you want to change it
                   )); ?>
      <h2>Вы искали "<span><?php echo CHtml::encode($search->string); ?></span>"</h2>
      <p class="res-count">Найдено <span><?=count($materials)?></span> <?=HtmlHelper::getNumberGood(count($materials), array('результат', 'результата', 'результатов'))?></p>
      <?if(count($materials)):?>
      <ul>
         <?php foreach($materials as $key => $result): ?>
            <li class="first clearfix">
               <div class="res-num left"><?=($key+1)?>.</div>
               <div class="res-text left">
                  <p class='res-title'><?php echo CHtml::link(CHtml::decode($result['title']), CHtml::encode($result['url']), array('class' => 'res-title')); ?></p>
                  <p class="res-body"><?php echo $result['text']; ?></p>
               </div>
            </li>
         <?php endforeach; ?>
      </ul>
      <?endif;?>
   </div>

</section>