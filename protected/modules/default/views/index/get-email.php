<section class="formBox">
    <div class="container">
        <div class="row">
            <article class="col-lg-8 col-md-8 col-sm-8 contactBox2">
                <h2><?= $this->pageTitle; ?></h2>
                <?php if(!Yii::app()->user->isGuest): ?>
                    <?php $form = $this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                    )); ?>
                    <?= $form->errorSummary($user); ?>
                    <div class="form-div-1 clearfix">
                        <label class="firstname">
                            <?= $form->textField($user, 'email', array(
                                'placeholder' => "Ваш Email:",
                            )) ?>
                        </label>
                    </div>
                    <div class="btns">
                        <input type="submit" value="Підтвердити" class="btn-default btn1"/>
                    </div>
                    <?php $this->endWidget(); ?>
                <?php endif; ?>

            </article>
        </div>
    </div>
</section>
