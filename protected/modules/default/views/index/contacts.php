<?php
/* @var $this CandidatesController */
/* @var $model Candidates */
/* @var $form CActiveForm */
?>
<section class="formBox col-lg-9 col-md-12 col-sm-12">
    <div class="container">
        <div class="row">
            <article class="col-lg-8 col-md-8 col-sm-8 contactBox2">
                <h2><?= $this->pageTitle; ?></h2>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <?= Yii::app()->user->getFlash('success') ?>
                <?php else: ?>
                    <?php $form = $this->beginWidget('FormWidget', array(
                        'id'=>'contact-form',
                        'enableAjaxValidation'=>false,
                    )); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="holder">
                        <div class="form-div-1 clearfix">
                            <?= $form->textFieldRow($model, 'name', array(
                                'data-constraints' => "@Required @JustLetters"));
                            ?>
                        </div>
                        <div class="form-div-2 clearfix">
                            <?= $form->textFieldRow($model, 'email', array(
                                'data-constraints' => "@Required @JustLetters"));
                            ?>
                        </div>
                        <div class="form-div-3 clearfix">
                            <?= $form->textFieldRow($model, 'subjects') ?>
                        </div>
                    </div>
                    <div class="form-div-5 clearfix">
                        <?= $form->textAreaFieldRow($model, 'text', array(
                            'data-constraints' => '@Required @Length(min=20,max=999999)'
                            )); ?>
                    </div>
                    <div class="btns">
                        <input type="submit" value="Вiдправити" class="btn-default btn1"/>
                    </div>
                    <?php $this->endWidget(); ?>
                <?php endif; ?>
            </article>
        </div>
    </div>
</section>