<?php if($text): ?>
    <article class="col-lg-12 col-md-12 col-sm-12">
        <div class="list_carousel1 responsive clearfix">
            <h2><?= $text->title ?></h2>
            <ul id="foo1">
                <?php /* ?><li><p><span></span><?= $text->content ?></p></li><?php */ ?>
                <li><?= $text->content ?></li>
            </ul>
            <div class="foo-btn clearfix">
                <div class="pagination" id="foo2_pag"></div>
            </div>
        </div>
    </article>
<?php endif; ?>