<section class="col-lg-9 col-md-9 col-sm-9 libBox">
    <div class="about">
        <h2><?= $text->title ?></h2>
        <div id="about-project">
            <?= $text->content ?>
        </div>
    </div>

    <h2>Нещодавно доданi</h2>
    <?php /*<a href="/candidates/" style="font-weight: bold;">Показати всiх</a>*/ ?>
    <div class="row">
        <?php foreach($lastAdded as $index => $data): ?>
            <?php $this->renderPartial('_view', array(
                'data' => $data, 
                'index' => $index,
                'user' => $user,
                'lg' => 4)); ?>
        <?php endforeach; ?>
    </div>
</section>

<?php $this->renderPartial('//partials/left-column') ?>

<section class="col-lg-12 col-md-12 col-sm-12 eventsBox">
    <h2>Всi кандидати на люстрацiю</h2>
    <div class="row">
        <?php
            $dataProvider = $dataProvider->search(false, 8, 'rand');
            $dataProvider->pagination->pageSize = 8;

            $this->widget('NewsListView', array(
                'dataProvider' => $dataProvider,
                'itemView'     => '_view',
                'ajaxUpdate'   => false,
                'viewData' => compact('user'),
                'pager' => array(
                    'header' => '',
                    'prevPageLabel'=>'<',
                    'nextPageLabel'=>'>',
                    'class' => 'Pager'
                ),
                'template'     => '{items}<div class="clearfix"></div>{pager}'
        )); ?>
    </div>
</section>
<?php $this->renderPartial('//partials/_popup', compact('user')); ?>