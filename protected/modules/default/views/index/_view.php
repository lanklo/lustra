<?php if(!isset($lg)) $lg = 3; ?>

<article class="col-lg-<?= $lg ?> col-md-4 col-sm-6 col-xs-6">
    <div class="thumb-pad1 lustra" candidate="<?= $data->id ?>">
        <div class="thumbnail">
            <div class="block-over">
                <figure>
                    <?php if(!empty($data->photo)): ?>
                        <div class="round-item">
                            <a class="round-hover" href="<?= $this->createUrl('/candidates/', array('view' => $data->id)) ?>"></a>
                            <?= CHtml::image($data->imageUrl) ?>
                        <? //= CHtml::link(CHtml::image($data->imageUrl),$this->createUrl('/default/candidates/view/', array('id' => $data->id))) ?>
                        </div>
                    <?php else: ?>
                        <?= CHtml::link('<span></span>',
                                $this->createUrl('/candidates/', array('view' => $data->id)),
                                array('style' => 'display:block; width:250px; height:180px')) ?>
                    <?php endif; ?>
                </figure>
                <div class="caption">
                    <span class="count"><?= $data->lustrationCount ?></span>
                    <div class="clearfix"></div><br/>
                    <?= CHtml::link($data->fullName, $this->createUrl('/candidates/', array('view' => $data->id))) ?>
                    <br clear="all"/><br clear="all"/><hr>
                    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="link" data-yashareQuickServices="vkontakte,facebook,twitter" data-yashareLink="<?= Yii::app()->getBaseUrl(true).$this->createUrl('/candidates/', array('view' => $data->id)) ?>" data-yashareTitle="Люстровано: <?= $data->fullName ?>" style="margin-bottom:12px;"></div>
                    <br clear="all"/><hr>
                    <p><?= $data->description ?></p>
                </div>
            </div>
            <?php $url = urlencode($this->createAbsoluteUrl('/') . $this->createUrl('/candidates/', array('view' => $data->id))); ?>
            <?= $this->renderPartial('//partials/_lustration', compact('data', 'user', 'url')) ?>
        </div>
    </div>
</article>