<?php
/* @var $this CategoriesController */
/* @var $model Categories */
?>
<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?=$this->pageTitle; ?></h1>
    </div>
</div>

<fieldset>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'filter-form',
        'type' => 'horizontal',
        'enableAjaxValidation'=>false,
        'method' => 'get',
        'action' => '/default/adminLustrations/getXls',
        'htmlOptions'=>array('class'=>'well')
    )); ?>

    <?= $form->dropDownList($model, 'candidate_id', $candidates, array('empty' => ' - Оберiть люстрованого - ')) ?>

    <?php /* $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>'Применить',
        'htmlOptions' => array('name' => 'save')));*/
    ?>

    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'htmlOptions' => array('name' => 'get-xls'),
        'label'=>'Выгрузить список в xlsx')
    ); ?>

    <?php $this->endWidget(); ?>
</fieldset>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'template'=>"{items}\n{pager}",
    'filter' => $model,
    'columns'=>array(
        array(
            'filter' => $candidates,
            'name' => 'candidate_id',
            'value' => '$data->candidateName',
        ),
        array(
            'filter' => false,
            'name' => 'user_id',
            'value' => '$data->userName',
        ),
        array(
            'filter' => false,
            'name' => 'method_id',
            'value' => '$data->methodName',
        ),
        array(
            'name' => 'UserEmail',
            'filter' => false
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'htmlOptions' => array('style'=>'width: 50px', 'class' => 'button-distance'),
            'template' => '{delete}',
        ),
    ),
));
