<?php

class AdminUsersController extends AdministrationController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('admin'),
            ),
            array('deny',
                    'users'=>array('*'),
                ),
        );
    }
    
    public function init()
    {
        Yii::app()->getComponent('bootstrap');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
            $model=$this->loadModel($id);
            $this->pageTitle = 'Оновити';

            if(isset($_POST['Users'])){
                $model->attributes=$_POST['Users'];
                if($model->save())
                    if(isset($_POST['apply']))
                        $this->redirect(array('update', 'id' => $model->id));
                    else $this->redirect(array('admin'));
            }

            $this->render('form',array(
                    'model'=>$model,
            ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            $this->loadModel($id)->delete();

            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
            $dataProvider=new CActiveDataProvider('Users');
            $this->render('index',array(
                    'dataProvider'=>$dataProvider,
            ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
            $model=new Users('search');
            $this->pageTitle = 'Управління користувачами';
            $model->unsetAttributes();  // clear any default values
            $model->role = 0;
            
            if(isset($_GET['Users']))
                    $model->attributes=$_GET['Users'];

            $this->render('admin',array(
                    'model'=>$model,
            ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Users the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
            $model=Users::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }

    public function actionXls()
    {
        $row = $this->_getData();
        $fields = array(
            array('Дата', 'create_time', '20'),
            array('Користувач', 'first_name', '35'),
            array('Email', 'email', '30'),
            array('Кого люстрував', 'candidate', '35'),
        );

        $fileHelper = new FileHelper();
        $fileHelper->getXls($fields, $row, 'lustration-load-statistic');
    }

    protected function _getData()
    {
        $data = Yii::app()->db->createCommand()
                ->select('l.id, l.create_time, u.first_name, u.email, c.firstname, c.lastname')
                ->from('lustrations l')
                ->leftJoin('users u', 'u.id = user_id')
                ->leftJoin('candidates c', 'c.id = candidate_id')
                ->where('u.social <> "anonim" AND u.email <>""')
                ->order('user_id, create_time')
                ->queryAll();

        foreach ($data as $k => $item){
            $data[$k]['create_time'] = date('d.m.Y H:i', CDateTimeParser::parse($item['create_time'],'yyyy-MM-dd hh:mm:ss'));
            $data[$k]['candidate'] = $item['lastname'] . ' ' . $item['firstname'];
        }
        return $data;
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            if(isset($_POST['ajax']) && $_POST['ajax']==='methods-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
    }
}
