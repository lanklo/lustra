<?php

class AdminLustrationsController extends AdministrationController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('admin'),
            ),
            array('deny',
                    'users'=>array('*'),
                ),
        );
    }
    
    public function init()
    {
        Yii::app()->getComponent('bootstrap');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            $this->loadModel($id)->delete();

            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $c = new CDbCriteria;
        $c->order = 'lastname';
        $candidates = CHtml::listData(Candidates::model()->findAll($c), 'id', 'fullname');
        $model=new Lustrations('search');
        $this->pageTitle = 'Управління люстрациями';
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Lustrations']))
                $model->attributes=$_GET['Lustrations'];

        $this->render('admin',array(
            'model'=>$model,
            'candidates' => $candidates,
        ));
    }

    public function actionGetXls()
    {
        $criteria=new CDbCriteria;
        if(isset($_GET['Lustrations'])){
            foreach ($_GET['Lustrations'] as $attr => $value){
                $criteria->compare($attr, $value);
            }
        }
        $row = Lustrations::model()->xls($criteria);
        
        $fields = array(
            array('id', 'id', '10'),
            array('Люстрований', 'candidateName', '35'),
            array('Хто люстрував', 'userName', '30'),
            array('Спосiб', 'methodName', '25'),
            array('Email', 'userEmail', '35'),
        );
        
        $fileHelper = new FileHelper();
        $fileHelper->getXls($fields, $row, 'load-stat-lustrations');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Methods the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
            $model=Lustrations::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }
}
