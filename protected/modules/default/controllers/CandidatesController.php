<?php

class CandidatesController extends Controller
{
	public function filters()
        {
            return array(
                'AnonymousUser + view, add, index',
                'BackEndSettings + create, update, admin, delete, requests',
                array('application.filters.AutoLustrateFilter + view, index'),
                'ajaxOnly + lustra',
            );
        }

	public function accessRules()
        {
            return array(
                array('allow',
                        'actions'=>array('index','view'),
                        'users'=>array('*'),
                ),
                array('deny',  // deny all users
                        'users'=>array('*'),
                ),
            );
        }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $this->_addJS();
            //$this->_addLustrationJs($id);
            $model = $this->loadModel($id);
            $this->pageTitle = $model->fullname;
            $this->_registerMetaTags($model);
            
            $methodsStatistic = $model->methodStatistic;
            
            $user = new Users();
            //$this->_anonim($user);
            
            if(!Yii::app()->user->isGuest){
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $this->_user($user);
                if(!$user->emailConfirmed)
                    Yii::app()->clientScript->registerScript('open', "$('#popUp').modal();");
            }
            $this->render('view', compact('model', 'methodsStatistic', 'user'));
	}

    public function actionAdd()
	{
            $this->_addJS();
            $user = new Users();
            //$this->_anonim($user);
            if(!Yii::app()->user->isGuest){
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $this->_user($user);
                if(!$user->emailConfirmed)
                    Yii::app()->clientScript->registerScript('open', "$('#popUp').modal();");
            }
            
            $model=new Candidates('front');
            $this->_registerMetatagsAdd();
            $this->pageTitle = 'Додати кандидата';
            $candidates = false;
            if(!Yii::app()->user->isGuest && isset($_POST['Candidates'])){
                $model->attributes = $_POST['Candidates'];
                $model->author_id = Yii::app()->user->id;
                $model->modarate = 0;
                if(!$user->email){
                    $model->addError('author_id', 'Спочатку заповнiть email');
                }else{
                    $candidates = $model->findCandidate();
                    if($model->validate()){
                        if(!$candidates && $model->save(false)){
                            if($model->author->social == 'vkontakte') {
                                Yii::app()->user->setFlash('lustration-vkontakte', 'Я хочу люструвати '.$model->firstname.' '.$model->lastname.'. А тобі є кого люструвати? Приєднуйся до всеукраїнської люстрації!');
                            }
                            else {
                                Yii::app()->user->setFlash('lustration-facebook', 'Я хочу люструвати '.$model->firstname.' '.$model->lastname.'. А тобі є кого люструвати? Приєднуйся до всеукраїнської люстрації!');
                            }

                            Yii::app()->user->setFlash('new-candidate',
                                'Дякуємо за Ваш внесок у процес Всеукраїнської люстрації!<br/>'
                                . 'Після перевірки інформації ми опублікуємо інформацію про нього на сайті.<br/>'
                                . 'Залучайте друзів - давайте активніше люструвати'
                            );
                            $url = Yii::app()->createAbsoluteUrl('/administration/default/update/mode/request/', array('id' => $model->id));
                            Mail::sendMail(
                                'Додан новий кандидат на люстрацiю',
                                "Додан новий кандидат на люстрацiю\n\n" .
                                "Iм'я: $model->firstname \n" .
                                "Прiзвище: $model->lastname \n\n" .
                                "Подивитись: $url\n",
                                Settings::getEmail('lustration')
                            );

                            $this->refresh();
                        }
                    }
                }
            }
            $categories = Categories::model()->findAll();
            $this->render('form-frontend', compact('model', 'candidates', 'user', 'categories'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($category_id = null, $search_string = null, $party_id = null)
	{
            $this->_addJS();
            $user = new Users();
            //$this->_anonim($user);
            if(!Yii::app()->user->isGuest){
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $this->_user($user);
                if(!$user->emailConfirmed)
                    Yii::app()->clientScript->registerScript('open', "$('#popUp').modal();");
            }
        
            $this->pageTitle = 'Люстрованi';
            $dataProvider = new Candidates;
            $dataProvider->modarate = 1;
            if ($category_id) $dataProvider->category_id = $category_id;
            if ($party_id) {
                if(!Parties::model()->exists('id=:id', array(':id' => $party_id)))
                    throw new CHttpException(404,'The requested page does not exist.');
                        
                $dataProvider->party_id = $party_id;
            }
            /*if ($_GET['search_string']) {
                $search_string = addcslashes($_GET['search_string'], '%_');
                $dataProvider->first_name = $search_string;
            }*/

            $this->render('index', compact('dataProvider', 'user', 'search_string'));
	}

        public function actionApprove($id, $remove = false)
        {
            if(Yii::app()->request->isAjaxRequest){
                $model = $this->loadModel($id);
                if($remove)
                    $model->remove();
                else
                    $model->approve();
            }
        }

        public function actionLustra()
        {
            if(Yii::app()->request->isAjaxRequest){
                $candidate_id = Yii::app()->request->getPost('candidate');
                $method_id = Yii::app()->request->getPost('method');
                //проверить люстрировал ли раньше
                $model = new Lustrations();
                //получить текущего юзера
                $model->user_id = Yii::app()->user->id;
                $model->candidate_id = $candidate_id;
                $model->method_id = $method_id;

                $exist = $model->exists('user_id=:user_id AND candidate_id=:candidate_id', array(
                    ':user_id' => $model->user_id,
                    ':candidate_id' => $model->candidate_id
                ));
                if($exist)
                    echo CJSON::encode(array('result' => 'error', 'message' => 'Ви вже люстровали'));
                elseif($model->save()){
                    $candidate = $this->loadModel($candidate_id);
                    $html = '';
                    foreach($candidate->methodStatistic as $item){
                        $html .= "<label>$item->title <span class='pers'";
                        if ($item->title == $candidate->punishment) {
                            $html .= ' style="font-weight: bold;"';
                        }
                        $html .= ">$item->persentage%</span></label><br/>";
                    }
                    $html .= '';
                    $userModel = Users::model()->findByAttributes(array('id' => Yii::app()->user->id));
                    echo CJSON::encode(array(
                        'result' => 'success',
                        'html' => $html,
                        'candidateName' => $candidate->fullName,
                        'lustrationsCount' => $candidate->getCandidateLustrations(),
                        'punishment' => $candidate->getPunishment(),
                        'socialType' => $userModel -> social,
                        'url' => $this->createAbsoluteUrl('/candidates/', array('view' => $candidate->id))
                    ));
                }
                
                Yii::app()->end();
            }
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Candidates the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Candidates::model()->with('lustrationCount')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Candidates $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='candidates-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

        protected function _lustration($candidate_id)
        {
            if(isset($_POST['Lustration'])){
                //проверить авторизацию

                //проверить люстрировал ли раньше
                $model = new Lustrations();
                $model->attributes = $_POST['Lustration'];

                //получить текущего юзера
                $model->user_id = Yii::app()->user->id;
                $model->candidate_id = $candidate_id;

                $exist = $model->exists('user_id=:user_id AND candidate_id=:candidate_id', array(
                    ':user_id' => $model->user_id,
                    ':candidate_id' => $model->candidate_id,
                ));
                if($exist){
                    Yii::app()->user->setFlash('lustration', 'Ви вже люстровали');
                }elseif($model->save()){
                    Yii::app()->user->setFlash('lustration', 'Спасибі');
                    if($model->user->social == 'vkontakte')
                        Yii::app()->user->setFlash('lustration-vkontakte', 'ls');
                    else
                        Yii::app()->user->setFlash('lustration-facebook', 'ls');
                }else{
                    throw new CHttpException(404,'The requested page does not exist.');;
                }
                $this->refresh();
            }
        }

        protected function _isLustra($candidate_id)
        {
            return Lustrations::model()->exists('user_id=:user_id AND candidate_id=:candidate_id', array(
                ':user_id' => Yii::app()->user->id,
                ':candidate_id' => $candidate_id
            ));
        }

        protected function _registerMetatags($model)
        {
            Yii::app()->clientScript->registerMetaTag(
                Yii::app()->request->hostInfo . $model->imageUrl, null, null, array('property' => 'og:image')
            );
            Yii::app()->clientScript->registerMetaTag(
                "Люстровано: {$model->fullname}. Покарання: {$model->punishment}.\n Приєднуйся до всеукраїнської люстрації!",
                null, null, array('property' => 'og:description')
            );
            Yii::app()->clientScript->registerMetaTag(
                '1598926791', null, null, array('property' => 'fb:admins')
            );
        }

        protected function _registerMetatagsAdd()
        {
            Yii::app()->clientScript->registerMetaTag(
                    Yii::app()->request->hostInfo . '/img/logo.png', null, null, array('property' => 'og:image')
            );
            Yii::app()->clientScript->registerMetaTag(
                    'Люстрацiя', null, null, array('property' => 'og:description')
            );
        }

    public function filterAnonymousUser($filterChain)
    {
        $user = new Users();
        if(isset($_POST['Anonim'])){
            $user->scenario = 'anonim';
//            $user->attributes = $_POST['Anonim'];
            $user->email = $this->_generateRandomString();
            $user->social = 'anonim';
            $user->last_name = 'confirmed';
            $user->sid = 111;
            $user->login = $user->email;

            if($user->save()){
                $identity = new CUserIdentity($user->id, '');
                Yii::app()->user->login($identity);
                $this->refresh();
            }else
                Yii::app()->clientScript->registerScript('open-anonim', "$('#anonim').modal();");
        }
        $filterChain->run();
    }

    /*
    protected function _anonim($user)
    {
        if(isset($_POST['Anonim'])){
            $user->scenario = 'anonim';
            $user->attributes = $_POST['Anonim'];
            $user->social = 'anonim';
            $user->last_name = 'confirmed';
            $user->sid = 111;
            $user->login = $user->email;

            if($user->save()){
                $identity = new CUserIdentity($user->id, '');
                Yii::app()->user->login($identity);
            }else
                Yii::app()->clientScript->registerScript('open-anonim', "$('#anonim').modal();");
        }
    }*/
        
    protected function _user($user)
    {
        if(isset($_POST['Users'])){
            $user->attributes = $_POST['Users'];
            $user->last_name = 'confirmed';
            if(!$user->email) $user->addError('email', 'Ви повинні ввести email');

            if($user->save()) $this->refresh();
            else
                Yii::app()->clientScript->registerScript('open', "$('#myModal').modal();");
        }
    }
    
    protected function _addJS()
    {
        $script = '$("input[type=radio]").click(function(){';
        $script.= '$(this).closest(".btm-block").find("a").removeAttr("disabled").removeClass("disabled");';
        if (Yii::app()->user->isGuest) {
            $script.= '$(this).closest(".btm-block").find("a").attr("data-target","#popUp")';
        }
        $script.= '});';

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/script.js');
        Yii::app()->clientScript->registerScript('radio',$script);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        if (Yii::app()->user->hasFlash('share-lustration')) {
            if (!Yii::app()->user->isGuest && $user->emailConfirmed) {
                Yii::app()->clientScript->registerScript('share-lustration',Yii::app()->user->getFlash('share-lustration'));
            }
        }
    }
    
    protected function _addLustrationJs($id)
    {
        if(!Yii::app()->user->isGuest){
            $url = urlencode($this->createAbsoluteUrl('/') . '/' . Yii::app()->request->pathInfo);
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $function = ($user->social == 'vkontakte') ? "vkshare('$url')" : "fbshare('$url')";
            Yii::app()->clientScript->registerScript('lustration-btn',
                "$('.lustruu').click(function(){
                    var obj = $('.lustruu');
                    var id = $id;
                    var method_id = $('input[name=\"Lustration[method_id][$id]\"]:checked').val();
                    if(!method_id) return;

                    $.ajax({
                        type: 'POST',
                        url: '/candidates/lustra',
                        data: {method: method_id, candidate: id},
                        success: function(data){
                            $(obj).parent().find('.text-left.methods').html(data.html);
                            $(obj).hide();
                            $(obj).parent().find('.share-block').show();
                            $function;
                        },
                        dataType: 'json'
                      });
                });"
            );
        }
    }

    protected function _generateRandomString($length = 7)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString . time();
    }
}
