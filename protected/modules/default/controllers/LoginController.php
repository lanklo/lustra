<?php
/**
 * Авторизация
 */
//Yii::import('application.modules.users.models.*');
class LoginController extends MainAppController
{
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha1'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xEEEEEE,
                'maxLength'=>3,
                'minLength'=>3
            ),
        );
    }

    public function actionIndex()
    {
        if (Yii::app()->request->isAjaxRequest) {
            header('Content-type: application/json');
            if (isset($_POST['Auth'])) {
                $auth = $_POST['Auth'];
                $auth['login'] = filter_var($auth['login'], FILTER_SANITIZE_SPECIAL_CHARS);
                $identity = new UserIdentity(array('user_name' => $auth['login'], 'password' => $auth['password']));
                if ($identity->authenticate()) {
                    if (isset($auth['save'])) {
                        $time = 3600 * 24;
                    } else {
                        $time = 0;
                    }
                    Yii::app()->user->login($identity, $time);
                    //$this->redirect("/administration");
                    echo CJSON::encode(array("status" => "success"));
                }
                else {
                    echo CJSON::encode(array("status" => "error", "message" => $identity->getError(), 'code' => $identity->errorCode));
                }
            }
            Yii::app()->end();
        } else {
            if (!Yii::app()->user->isGuest) {
                $this->redirect("/administration");
            }
        }

        $this->renderPartial('login');
    }

    public function actionAuth()
    {
        if (!Yii::app()->user->isGuest) {
            $this->redirect("/administration");
        }
        
        if (isset($_POST['Auth'])) {
            $auth = $_POST['Auth'];
//            if (Yii::app()->session['secpic'] == $auth['key']) {
                $auth['login'] = filter_var($auth['login'], FILTER_SANITIZE_SPECIAL_CHARS);
                $identity = new UserIdentity(array('user_name' => $auth['login'], 'password' => $auth['password']));
                if ($identity->authenticate()) {
                    Yii::app()->user->login($identity, 3600 * 24);
                    $this->redirect("/administration");
                }
                else {
                    echo "Not auth";
                }
//            }
//            else {
//                echo "Not key";
//            }
        }

        $this->renderPartial('login');
    }


    public function actionSocial()
    {
        $type = $this->_getTypeSocial($_GET);
        if ($type != 'error' || isset($_GET['code'])) {

            $code = $_GET['code'];
            $socialModel = $this->_getObjectSocial($type);
            $socialModel->getAccessToken($code);
            $socialId = $socialModel->getUserId();
            if ($socialId !== null) {
                $identity = new UserIdentity(array('social_id' => $socialId,'type'=>$type));
                if (!$identity->authenticate()) {
                    $data = $socialModel->getUserData();
                    $data['social_id'] = $socialId;
                    Yii::app()->session['social'] = $data;
                    echo '<script type="text/javascript">window.opener.location.href="/' . Yii::app()->language . '/default/registration";window.close();</script>';
                } else {
                    Yii::app()->user->login($identity, 3600 * 24);
                    echo '<script type="text/javascript">window.opener.location.reload(true);window.close();</script>';
                }
            } else {
                echo '<script type="text/javascript">window.close();</script>';
            }

        }
        Yii::app()->end();
    }

    public function actionExit()
    {
        // Выходим
        Yii::app()->user->logout();
        $this->redirect("/");
    }

    public function actionCaptcha()
    {
        $cp = new Captcha();
        $cp->genCaptcha(114, 33, 14, 4);
    }
    
    public function actionSendCode()
    {
        if (Yii::app()->request->isAjaxRequest) {
            header('Content-type: application/json');
            $phone = Yii::app()->request->getParam('phone', false);
            if ($phone === false) {
                echo CJSON::encode(array("status" => "error","message"=>Yii::t('registration','Введите телефон в международном формате')));
            } else {
                $model = Users::model()->findByAttributes(array('phone' => $phone));
                if($model===null){
                    echo CJSON::encode(array("status" => "error","message"=>Yii::t('registration','Телефон не найден')));
                }elseif($model->social_id!='' && $model->login==''){
                    echo CJSON::encode(array("status" => "success",'back'=>1,"message"=>Yii::t('registration','Ваш аккаунт привязан к соц сети')));
                }else{
                    $userIp = (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '127.0.0.1')
                        ? $_SERVER['HTTP_X_FORWARDED_FOR']
                        : Yii::app()->request->getUserHostAddress();
                    $code = NumGeneratorHelper::generate(6);
                    $params = array();
                    $params['phone'] = $phone;
                    $params['ip'] = $userIp;
                    $params['text'] = $code;
                    $sms = new Sms();
                    //$t =1;
                    if ($sms->sendSms($params)) {
                        $remind = new UserRemind();
                        $remind->user_id = $model->id;
                        $remind->code = $code;
                        $remind->status = 'wait';
                        $remind->date = date("Y-m-d H:i:s");
                        $remind->save();
                        echo CJSON::encode(array("status" => "success",'message'=>Yii::t('registration','Код успешно выслан')));
                    }else{
                        echo CJSON::encode(array("status" => "error", "message" => $sms->getError()));
                    }
                }
            }
        Yii::app()->end();
        }else{
            $this->redirect('/'.Yii::app()->language);
        }
    }

    public function actionChangePassword()
    {
        if (Yii::app()->request->isAjaxRequest) {
            header('Content-type: application/json');
            if(isset($_POST['Remind'])){
                $form = new ChangePasswordForm();
                $form->attributes = $_POST['Remind'];
                if($form->validate()){
                    $remind = UserRemind::model()->findByAttributes(array('code'=>$form->code),"status='wait'");
                    if($remind && (time()-strtotime($remind->date))< 20*60){
                        $remind->users->password = md5($remind->users->salt . md5($_POST["Remind"]["password"]));
                        $remind->users->save(false);
                        $remind->status = 'active';
                        $remind->save();
                        echo CJSON::encode(array("status" => "success",'message'=>Yii::t('registration','Пароль успешно изменен')));
                    }else{
                        echo CJSON::encode(array("status" => "error",'message'=>array('code'=>Yii::t('registration','Код не найден в системе'))));
                    }
                }else{
                    echo CJSON::encode(array("status" => "error",'message'=>$form->getErrors()));
                }
            }else{
                echo CJSON::encode(array("status" => "error"));
            }
            Yii::app()->end();
        }else{
            $this->redirect('/'.Yii::app()->language);
        }
    }

    protected function _getTypeSocial($request_params)
    {
        $social_type = array('vk', 'fb');
        $type = null;
        foreach ($request_params as $k => $p) {
            if (in_array($k, $social_type)) {
                $type = $k;
                break;
            }
            if ($k == 'error') {
                $type = 'error';
                break;
            }
        }
        return $type;
    }

    protected function _getObjectSocial($type)
    {
        switch ($type) {
            case 'vk':
                return new Vkontakte();
                break;
            case 'fb':
                return new Facebook();
                break;
        }
    }

}