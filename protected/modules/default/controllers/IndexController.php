<?php

class IndexController extends Controller
{
    public function filters()
    {
        return array(
            'AnonymousUser + index',
            array('application.filters.AutoLustrateFilter + index'),
            'ajaxOnly + emailConfirm',
        );
    }
    /**
     * Lists all news
     */
    public function actionIndex()
    {
        $this->_addJS();
        $this->_registerMetatags();
        $user = new Users();
/*        $this->_anonim($user);*/
        if(!Yii::app()->user->isGuest){
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $this->_user($user);
            if(!$user->emailConfirmed)
                Yii::app()->clientScript->registerScript('open', "$('#popUp').modal();");
        }
        
        $this->pageTitle = 'Головна';
        $lastAdded = Candidates::model()->lastAdded(3)->findAll();
        $dataProvider = new Candidates;
        $dataProvider->modarate = 1;

        $text = Statpage::model()->getText();

        $this->render('index', compact('dataProvider', 'lastAdded', 'text', 'user'));
    }
    
    public function actionLogin($url = null)
    {
        $serviceName = Yii::app()->request->getQuery('service');

        if (isset($serviceName)) {
            /** @var $eauth EAuthServiceBase */
            $eauth = Yii::app()->eauth->getIdentity($serviceName);
//            $eauth->redirectUrl = $url ? urldecode($url) : "/"; //Yii::app()->user->returnUrl;
            $eauth->redirectUrl = Yii::app()->user->returnUrl;
            $eauth->cancelUrl = $this->createAbsoluteUrl('/');
            
//            if($url) $eauth->redirectUrl = $url;
            try {
                if ($eauth->authenticate()) {
                    $identity = new EAuthUserIdentity($eauth);
                    // successful authentication
                    if ($identity->authenticate()){
                        Yii::app()->user->login($identity);
                        $user = Users::model()->findByAttributes(array(
                            'social' => $serviceName,
                            'sid' => $identity->id
                        ));
                        if($user === null){
                            $user = new Users();
                            $user->sid = $identity->id;
                            $user->social = $serviceName;
                            $user->first_name = $identity->name;
                            $user->email = $identity->email;
                            
                            if(!$user->save())
                                throw new CHttpException(404,'The requested page does not exist.');
                            
                        }
                        Yii::app()->user->id = $user->id;
                        Yii::app()->user->name = $user->first_name;
                        Yii::app()->user->email = $user->email;
                        Yii::app()->user->social = $user->social;

                        // special redirect with closing popup window
                        if($serviceName) {
                            $eauth->redirect($url);
                        }
                    }
                    else {
                        $eauth->cancel();
                    }
                }
//                $this->redirect(array('site/login'));
            }
            catch (EAuthException $e) {
                Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
    }

    public function actionEmailConfirm() {
        if(Yii::app()->request->isAjaxRequest){
            $method = Yii::app()->request->getPost('method');
            if ($method == 'cancel') {
                //$user = new Users();
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $user->last_name = 'canceled';
                $user->save();
            }
            Yii::app()->end();
        }
    }

    public function actionContacts()
    {
        $model = new Contacts();
        $this->pageTitle = 'Контакти';

        if(isset($_POST['Contacts'])){
            $model->attributes = $_POST['Contacts'];
            if($model->save()){
                Yii::app()->user->setFlash('success', 'Дякуємо за ваше повідомлення.');
                
                Mail::sendMail('Нове повiдомлення з форми Контакти',
                    "Було вiдправлено нове повiдомлення\n\n" .
                    "Iм'я: $model->name\n" .
                    "Email: $model->email\n" .
                    "Тема: $model->subjects\n" .
                    "Текст повiдомлення: $model->text",
                    Settings::getEmail('feedback')
                );
                $this->refresh();
            }
            $model->name = htmlspecialchars_decode($model->name);
            $model->text = htmlspecialchars_decode($model->text);
            $model->subjects = htmlspecialchars_decode($model->subjects);
        }

        $this->render('contacts', array('model' => $model));
        
    }

    public function filterAnonymousUser($filterChain)
    {
        $user = new Users();
        if(isset($_POST['Anonim'])){
            $user->scenario = 'anonim';
            $user->attributes = $_POST['Anonim'];
            $user->social = 'anonim';
            $user->last_name = 'confirmed';
            $user->sid = 111;
            $user->login = $user->email;

            if($user->save()){
                $identity = new CUserIdentity($user->id, '');
                Yii::app()->user->login($identity);
            }else
                Yii::app()->clientScript->registerScript('open-anonim', "$('#anonim').modal();");
        }
        $filterChain->run();
    }
    
    protected function _user($user)
    {
        if(isset($_POST['Users'])){
            $user->attributes = $_POST['Users'];
            $user->last_name = 'confirmed';
            if(!$user->email) $user->addError('email', 'Ви повинні ввести email');

            if($user->save()) $this->refresh();
            else
                Yii::app()->clientScript->registerScript('open', "$('#myModal').modal();");
        }
    }
    
    protected function _addJS()
    {
        $script = '$("input[type=radio]").click(function(){';
        $script.= '$(this).closest(".btm-block").find("a").removeAttr("disabled").removeClass("disabled");';
        if (Yii::app()->user->isGuest) {
            $script.= '$(this).closest(".btm-block").find("a").attr("data-target","#popUp")';
        }
        $script.= '});';

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/script.js');
        Yii::app()->clientScript->registerScript('radio',$script);

        $user = Users::model()->findByPk(Yii::app()->user->id);
        if (Yii::app()->user->hasFlash('share-lustration')) {
            if (!Yii::app()->user->isGuest && $user->emailConfirmed) {
                Yii::app()->clientScript->registerScript('share-lustration',Yii::app()->user->getFlash('share-lustration'));
            }
        }
    }

    protected function _registerMetatags()
    {
        Yii::app()->clientScript->registerMetaTag(
            Yii::app()->request->hostInfo . "/img/logo-social.png", null, null, array('property' => 'og:image')
        );
        Yii::app()->clientScript->registerMetaTag(
            "Приєднуйся до всеукраїнської люстрації!",
            null, null, array('property' => 'og:description')
        );
    }
}
