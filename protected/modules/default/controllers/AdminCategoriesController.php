<?php

class AdminCategoriesController extends AdministrationController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('admin'),
            ),
            array('deny',
                    'users'=>array('*'),
                ),
        );
    }
    
    public function init()
    {
        Yii::app()->getComponent('bootstrap');
    }
    
    public function actionCreate()
    {
            $model=new Categories;
            $this->pageTitle = 'Додати категорію';
            if(isset($_POST['Categories']))
            {
                $model->attributes=$_POST['Categories'];
                if($model->save())
                    if(isset($_POST['apply']))
                        $this->redirect(array('update', 'id' => $model->id));
                    else $this->redirect(array('admin'));
            }

            $this->render('_form',array(
                    'model'=>$model,
            ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Оновити категорію';
        $model=$this->loadModel($id);

        if(isset($_POST['Categories'])){
            $model->attributes=$_POST['Categories'];
            if($model->save())
                if(isset($_POST['apply']))
                    $this->redirect(array('update', 'id' => $model->id));
                else $this->redirect(array('admin'));
        }

        $this->render('_form',array(
                'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            $this->loadModel($id)->delete();

            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
            $dataProvider=new CActiveDataProvider('Categories');
            $this->render('index',array(
                    'dataProvider'=>$dataProvider,
            ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->pageTitle = 'Управління категоріями';
        $model=new Categories('search');
        $model->unsetAttributes();  
        if(isset($_GET['Categories']))
                $model->attributes=$_GET['Categories'];

        $this->render('admin',array(
                'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Categories the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
            $model=Categories::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Categories $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
    }
}
