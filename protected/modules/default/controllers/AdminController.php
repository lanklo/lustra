<?php

class AdminController extends AdministrationController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('admin'),
            ),
            array('deny',
                    'users'=>array('*'),
                ),
        );
    }
    
    public function init()
    {
        Yii::app()->getComponent('bootstrap');
    }
    
    public function actionIndex()
    {
        $this->render('index');
    }
    
    public function actionAdmin()
    {
        $this->pageTitle = 'Управлiння кандидатами';
        $model=new Candidates('search');
        $model->unsetAttributes();  // clear any default values
        $model->modarate = 1;
        if(isset($_GET['Candidates']))
                $model->attributes=$_GET['Candidates'];

        $this->render('requests',array(
                'model'=>$model,
        ));
    }
    
    public function actionRequests()
    {
        $this->pageTitle = 'Управлiння заявками';
        $model=new Candidates('search');
        $model->unsetAttributes();  // clear any default values
        $model->modarate = 0;
        if(isset($_GET['Candidates']))
                $model->attributes=$_GET['Candidates'];

        $this->render('requests',array(
                'model'=>$model,
                'mode' => 'request'
        ));
    }
    
    public function actionCreate()
    {
        $model=new Candidates;
        $model->modarate = 1;
        $this->pageTitle = 'Додати кандидата';

        if(isset($_POST['Candidates'])){
            $model->attributes=$_POST['Candidates'];
            if($model->save())
                if(isset($_POST['apply']))
                    $this->redirect(array('update', 'id' => $model->id));
                else $this->redirect(array('admin'));
        }

        $this->render('form',array(
            'model'=>$model,
        ));
    }
    
    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $this->pageTitle = 'Оновити кандидата';

        if(isset($_POST['Candidates']))
        {
            $model->attributes=$_POST['Candidates'];
            if($model->save())
                if(isset($_POST['apply']))
                    $this->redirect(array('update', 'id' => $model->id));
                else $this->redirect(array('admin'));
        }

        $this->render('form',array(
                'model'=>$model,
        ));
    }
    
    public function actionApprove($id, $remove = false)
    {
        if(Yii::app()->request->isAjaxRequest){
            $model = $this->loadModel($id);
            if($remove)
                $model->remove();
            else
                $model->approve();
        }

    }

     public function actionStatpageUpdate($id)
    {
        $this->pageTitle = 'Обновить страницу';
        $model = Statpage::model()->findByPk($id);
        $class = get_class($model);

        if (isset($_POST[$class])) {
            $model->attributes = $_POST[$class];

            if ($model->save()) {
                $this->redirect(array('settings'));
            }
            $model->title = htmlspecialchars_decode($model->title);
        }

        $this->render('_statform', compact('model'));
    }
    
    public function actionSettings()
    {
        $this->pageTitle = 'Налаштування';
        $dataProvider = Statpage::model()->search();
        $emailProvider = Settings::model()->search();
        
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $user->setScenario('admin-settings');
        $this->_changePassword($user);

        $this->render('settings', compact('dataProvider', 'emailProvider', 'user'));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax'])){
                if(isset($_GET['mode']) && $_GET['mode'] == 'request')
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('requests'));
                else
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
    }

    public function actionEmailUpdate($code)
    {
        $model = Settings::model()->findByPk($code);
        $this->pageTitle = 'Оновити Email';

        if(isset($_POST['Settings']))
        {
            $model->attributes=$_POST['Settings'];
            if($model->save()) $this->redirect(array('settings'));
        }

        $this->render('form-email',array(
                'model'=>$model,
        ));
    }
    
    public function loadModel($id)
    {
            $model=Candidates::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }
    
    protected function _changePassword($model)
    {
        if(isset($_POST['Users'])){
            $model->attributes = $_POST['Users'];
            if($model->save())
                $this->refresh ();
        }
    }
}
