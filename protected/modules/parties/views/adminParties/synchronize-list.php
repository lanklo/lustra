<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left">Синхронизация</h1>
    </div>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'action' => $this->createUrl('sinhronize'),
    )) ?>
        <?= $form->fileFieldRow($pModel, 'file', array('accept' => 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')) ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Загрузить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
    <?php $this->endWidget(); ?>
    ИЛИ <?= CHtml::link("Синхронизировать с последним загруженным файлом", "/parties/adminParties/sinhronizeLastFile") ?>
</div>

<?php if(isset($candidates)): ?>
    <div class="row-fluid">
        <div class="page-header container-fluid">
            <h1 class="pull-left">Отчет о синхронизации</h1>
        </div>
    </div>
    <div class="row-fluid" id="grid">
        <h4>Кандидаты, которым присвоена партия</h4>
        <p>Количество кандидатов, у которых была обновлена партия: <?= count($candidates) ?></p>
        <?php foreach ($candidates as $item): ?>
            Кандидат: <?= CHtml::link($item->fullname, "/administration/default/update/id/$item->id", array('target' => '_blank')) ?>,
            присвоена партия - <?= $item->partyName ?><br/>
        <?php endforeach; ?>
        <?php if(count($newCandidates)): ?>
            <h4>Члены партий, которых нет в списке люстрируемых</h4>
            <?php foreach($newCandidates as $item): ?>
                Кандидат: <?= $item->fullname ?>, <?= $item->partyName ?><br/>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>