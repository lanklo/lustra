<article class="col-lg">
    <div class="thumb-pad1">
        <div class="thumbnail">
            <div class="col-lg-4 text-left">
                <?php if(!empty($data->img)): ?>
                    <?= CHtml::image($data->imageUrl) ?>
                <?php endif; ?>
            </div>
            <div class="col-lg-8 text-left">
                <h2><?= $data->title ?></h2>
                Всього люстровано кандидатiв: <?= $data->countC ?>
                <?php $this->widget('RankWidget', array(
                    'title' => 'Рейтинг ганебностi',
                    'allCount' => $data->count,
                    'class' => 'red',
                    'rate' => $data->countC,
                    'maxCount' => $maxCount,
                )) ?>
                <? //= is_object($data->candidates) ? $data->candidates->lustrationCount : 0 ?><br/>
                <?= CHtml::link('Переглянути всiх',
                        $this->createUrl('/candidates/', array('party_id' => $data->id)),
                        array('class' => 'white-link')
                ) ?><br/>
            </div>
        </div>
    </div>
</article>