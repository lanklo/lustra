<?php $maxCount = isset($parties[0]) ? $parties[0]->countC : 0; ?>
<article class="col-lg-9 col-md-12 col-sm-12 parties">
        <div class="about">
            <h2><?= $text->title ?></h2>
            <div id="about-project">
                <?= $text->content ?>
            </div>
        </div>
        <h2>Рейтинг ганебності партій</h2>
        <article class="col-lg">
            <div class="thumb-pad1">
                <?php foreach($parties as $k => $data): ?>
                <div class="thumbnail">
                    <div class="col-lg-2 text-left">
                        <?php if(!empty($data->img)): ?>
                            <?= CHtml::image($data->imageUrl, '', array('class' => 'small-img')) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-10 text-left">
                        <?php $this->widget('RankWidget', array(
                            'allCount' => $data->count,
                            'class' => 'black',
                            'col' => 9,
                            'title' => $data->title,
                            'rate' => $data->countC,
                            'index' => $k,
                            'maxCount' => $maxCount
                        )) ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php endforeach; ?>
            </div>
        </article>
        <div class="clearfix"></div>
        <div class="col-lg-12 text-left thumb-pad1">
            <?= $this->renderPartial('//partials/_likebox') ?>
        </div>
        <div class="clearfix"></div>
        <?php
            $this->widget('NewsListView', array(
            'dataProvider' => $dataProvider->published()->search(),
            'viewData' => array('maxCount' => $maxCount),
            'itemView'     => '_view',
            'ajaxUpdate'   => false,
            'pager' => array(
                'header' => '',
                'prevPageLabel'=>'<',
                'nextPageLabel'=>'>',
                'class' => 'Pager'
            ),
            'template'     => '{items}<div class="clearfix"></div>{pager}'
        )); ?>
</article>