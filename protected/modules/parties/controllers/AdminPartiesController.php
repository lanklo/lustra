<?php

class AdminPartiesController extends AdministrationController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('admin'),
            ),
            array('deny',
                    'users'=>array('*'),
                ),
        );
    }
    
    public function init()
    {
        Yii::app()->getComponent('bootstrap');
    }
    
    public function actionCreate()
    {
            $model=new Parties;
            $this->pageTitle = 'Додати партiю';
            if(isset($_POST['Parties']))
            {
                $model->attributes=$_POST['Parties'];
                if($model->save())
                    if(isset($_POST['apply']))
                        $this->redirect(array('update', 'id' => $model->id));
                    else $this->redirect(array('admin'));
            }

            $this->render('_form',array(
                    'model'=>$model,
            ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Оновити партiю';
        $model=$this->loadModel($id);

        if(isset($_POST['Parties'])){
            $model->attributes=$_POST['Parties'];
            if($model->save())
                if(isset($_POST['apply']))
                    $this->redirect(array('update', 'id' => $model->id));
                else $this->redirect(array('admin'));
        }

        $this->render('_form',array(
                'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
            $this->loadModel($id)->delete();

            if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
            $dataProvider=new CActiveDataProvider('Parties');
            $this->render('index',array(
                    'dataProvider'=>$dataProvider,
            ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->pageTitle = 'Управління партiями';

        $model=new Parties('search');
        $model->unsetAttributes();
        if(isset($_GET['Parties']))
                $model->attributes=$_GET['Parties'];

        $this->render('admin',array(
                'model'=>$model
        ));
    }

    public function actionSinhronize()
    {
        $pModel = new PartyFile();
        if(isset($_POST['PartyFile'])){
            $file = CUploadedFile::getInstance($pModel, 'file');
            $file->saveAs('party-list' . preg_replace('/(^.*)(\.)/', '$2', $file->name));

            $result = $this->_sinhronizeWithPartiesFile();
            extract($result);
        }
        $this->render('synchronize-list', compact('candidates', 'newCandidates', 'pModel'));
    }

    public function actionSinhronizeLastFile()
    {
        $pModel = new PartyFile();

        $result = $this->_sinhronizeWithPartiesFile();
        extract($result);

        $this->render('synchronize-list', compact('candidates', 'newCandidates', 'pModel'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Parties the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
            $model=Parties::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Parties $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
            if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
    }

    protected function _getName($item)
    {
        $fullname = explode(' ', trim($item));
        return array(
            'lastname' => isset($fullname[0]) ? $fullname[0] : '',
            'firstname' => isset($fullname[1]) ? $fullname[1] : '',
            'secondname' => isset($fullname[2]) ? $fullname[2] : '',
        );
    }

    protected function _updateCandidate($model)
    {
        //проставить партию люстрированным
        $c = new CDbCriteria;
        $c->condition = 'firstname=:firstname AND lastname=:lastname';
        $c->params = array(
            ':firstname' => $model->firstname,
            ':lastname' => $model->lastname,
        );
        if($model->secondname){
            $c->addCondition('secondname=:secondname');
            $c->params[':secondname'] = $model->secondname;
        }
        $candidate = Candidates::model()->find($c);
        if($candidate){
            $candidate->setScenario('sync');
            $candidate->party_id = $model->party_id;
            $candidate->update(array('party_id'));
            return $candidate;
        }
        return false;
    }

    private function _sinhronizeWithPartiesFile() {
        $xls = new Xls();
        $sheetData = $xls->readXls();

        $candidates = array();
        $newCandidates = array();
        foreach($sheetData as $k => $item){
            $model = new Partylist();
            if($item['A']){
                $fullname = $this->_getName($item['A']);
                $model->lastname = $fullname['lastname'];
                $model->firstname = $fullname['firstname'];
                $model->secondname = $fullname['secondname'];
                $model->partyName = $item['B'];
                $model->save();

                $candidate = $this->_updateCandidate($model);
                if($candidate){
                    $candidates[] = $candidate;
                }else{
                    $newCandidates[] = $model;
                }
            }
        }
        return compact('candidates', 'newCandidates');
    }
}
