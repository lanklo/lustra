<?php

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $text = Statpage::model()->getText('party');
        
        $this->pageTitle = 'Партії';
        $c = new CDbCriteria;
        $c->select = '*, (SELECT COUNT(*) FROM candidates l WHERE l.party_id = t.id AND (SELECT COUNT(id) FROM lustrations WHERE candidate_id = l.id) > 0) as countC';
        $c->order = 'countC desc';
        $parties = Parties::model()->published()->findAll($c);
        $dataProvider = new Parties();
        $this->render('index', compact('dataProvider', 'parties', 'text'));
    }
}