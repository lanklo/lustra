<?php

class PartyFile extends CFormModel
{
        public $file;
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('file', 'file',
                            'types' => 'xls, xlsx',
                            'allowEmpty' => true
                        ),
		);
	}

        public function attributeLabels()
	{
		return array(
			'file' => 'Загрузите файл для синхронизации',
		);
	}
}
