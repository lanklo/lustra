<?php

/**
 * This is the model class for table "partylist".
 *
 * The followings are the available columns in table 'partylist':
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $secondname
 * @property integer $party_id
 */
class Partylist extends CActiveRecord
{
        public $partyName;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partylist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('firstname, lastname, secondname, party_id', 'required'),
			array('party_id', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname, secondname', 'length', 'max'=>255),
                        array('firstname, lastname, secondname', 'filter', 'filter'=>array($this, 'cleanAttribute')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, firstname, lastname, secondname, party_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'party' => array(self::BELONGS_TO, 'Parties', 'party_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstname' => "Iм'я",
			'lastname' => 'Прізвище',
			'secondname' => 'По батьковi',
			'party_id' => 'Партiя',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('secondname',$this->secondname,true);
		$criteria->compare('party_id',$this->party_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partylist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function beforeSave()
        {
            parent::beforeSave();
            if($this->scenario != 'insert')
                return;
            
            if(!empty($this->partyName)){
//                $party = Parties::model()->findByAttributes(array('title' => $this->partyName));
                $party = Parties::model()->find('LOWER(title) = :title', array(':title' => strtolower($this->partyName)));
                if($party === null){
                    $party = new Parties('synchronize');
                    $party->title = $this->partyName;
                    $party->save();
                }

                $this->party_id = $party->id;
            }
            
            $c = new CDbCriteria;
            $c->condition = 'firstname=:firstname AND lastname=:lastname';
            $c->params = array(
                ':firstname' => $this->firstname,
                ':lastname' => $this->lastname,
            );
            if($this->secondname){
                $c->addCondition('secondname=:secondname');
                $c->params[':secondname'] = $this->secondname;
            }
            $ex = self::model()->find($c);
            if($ex){
                $ex->party_id = $this->party_id;
                $ex->update(array('party_id'));
                return false;
            }
            return true;
        }

        public function getFullname()
        {
             $name = $this->lastname . ' ' . $this->firstname;
             if($this->secondname) $name .= ' ' . $this->secondname;
             return $name;
        }

        public function getPartyName()
        {
            return $this->party->title;
        }

        public function cleanAttribute($str)
        {
            return trim($str);
        }
}
