<?php

/**
 * This is the model class for table "parties".
 *
 * The followings are the available columns in table 'parties':
 * @property integer $id
 * @property boolean $active
 * @property string $title
 * @property string $img
 */
class Parties extends CActiveRecord
{
        protected $_path = "parties-photos/";
        public $lustrationCount;
        public $countC;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'parties';
	}

        public function scopes()
        {
            parent::scopes();

            return array(
                'published' => array(
                    'condition' => 'active = 1',
                )
            );
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
                        array('active', 'boolean'),
			array('title', 'length', 'max'=>255),
                        array('img', 'file',
                            'allowEmpty' => true,
                            'types' => 'jpg, jpeg, gif, png',
                            'maxSize' => 1024 * 1024 * 1
                        ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'candidates' => array(self::HAS_MANY, 'Candidates', 'party_id'),
                    'count' => array(self::STAT, 'Candidates', 'party_id'),
//                    'countCc' => array(self::STAT, 'Candidates', 'party_id', 'condition' => 'Candidates.lustrationCount > 0'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Назва',
                        'active' => 'Активнiсть',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
//                $criteria->with('candidate')
//                $criteria->select = '*, (SELECT COUNT(*) FROM lustrations l WHERE l.candidate_id IN (SELECT id FROM candidates WHERE party_id = t.id)) as lustrationCount';
                $criteria->select = '*, (SELECT COUNT(*) FROM candidates l WHERE l.party_id = t.id AND (SELECT COUNT(id) FROM lustrations WHERE candidate_id = l.id) > 0) as countC';
                $criteria->order = 'countC desc';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function search2()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
//                $criteria->with('candidate')
//                $criteria->select = '*, (SELECT COUNT(*) FROM lustrations l WHERE l.candidate_id IN (SELECT id FROM candidates WHERE party_id = t.id)) as lustrationCount';
                $criteria->select = '*, (SELECT COUNT(*) FROM candidates l WHERE l.party_id = t.id AND (SELECT COUNT(id) FROM lustrations WHERE candidate_id = l.id) > 0) as countC';
                $criteria->order = 'countC desc';
                $criteria->limit = 6;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
//                        'pagination' => array(
//                            'pageSize' => 6
//                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Parties the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function beforeSave()
        {
            if(parent::beforeSave()) {
                $this->_deleteImage();
                $this->_uploadFile('img', 255);

                return true;
            } else
                return false;
        }

        protected function beforeDelete()
        {
            if(!parent::beforeDelete())
                return false;

            $this->deleteFile('img');
            return true;
        }

        public function getCountCc()
        {
            $sql = "(t.party_id = $this->id AND (SELECT COUNT(id) FROM lustrations WHERE candidate_id = t.id) > 0)";
            $cont = Candidates::model()->count($sql);
            return $cont;
        }

        /*** img ***/
        public function deleteFile($attr)
        {
            $this->_deleteFile($attr, true);
        }

        public function getImageUrl()
        {
            return $this->_getBaseImagePath() . 't/' . $this->img;
        }

        private function _getBaseImagePath()
        {
            return Yii::app()->request->baseUrl . '/' . $this->_path;
        }

        private function _getImagePath()
        {
            return YiiBase::getPathOfAlias("webroot").DIRECTORY_SEPARATOR.$this->_path;
        }

        protected function _deleteFile($attr, $thumb = false)
        {
            $documentPath = $this->_getImagePath(). DIRECTORY_SEPARATOR . $this->$attr;

            if(is_file($documentPath)) unlink($documentPath);

            if($thumb){
                $documentPath = $this->_getImagePath(). DIRECTORY_SEPARATOR . 't' . DIRECTORY_SEPARATOR . $this->$attr;
                if(is_file($documentPath)) unlink($documentPath);
            }

            $this->$attr = null;
        }

        protected function _uploadFile($attr, $width = null, $height = null)
        {
            if(($this->scenario=='insert' || $this->scenario=='update') &&
                ($file = EUploadedImage::getInstance($this, $attr)) && $file->name){
                $this->deleteFile($attr);
                $this->$attr = rand() . time() . preg_replace('/(^.*)(\.)/', '$2', $file->name);

                $folder = $this->_getImagePath();
                if (!file_exists($folder))
                    mkdir($folder, 0, true);

                $file->saveAs($folder . DIRECTORY_SEPARATOR . $this->$attr);

                $file->reset();
                $file->maxWidth = $width;

                if (!file_exists($folder .DIRECTORY_SEPARATOR . 't'))
                    mkdir($folder .DIRECTORY_SEPARATOR . 't', 0, true);

                $file->saveAs($folder .DIRECTORY_SEPARATOR . 't' . DIRECTORY_SEPARATOR . $this->$attr);
            }
        }

        protected function _deleteImage()
        {
            if(isset($_POST[get_class($this)]['delete']))
                foreach($_POST[get_class($this)]['delete'] as $attr => $value){
                    if ($value) $this->deleteFile($attr);
                }
        }
}
