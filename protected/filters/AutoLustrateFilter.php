<?php

class AutoLustrateFilter extends CFilter {
    protected function preFilter($filterChain)
    {
        if (isset(Yii::app()->request->cookies['lustration_candidate'])
            && isset(Yii::app()->request->cookies['lustration_method'])
        ) {
            $candidateId = Yii::app()->request->cookies['lustration_candidate']->value;
            $methodId = Yii::app()->request->cookies['lustration_method']->value;

            $model = new Candidates();
            try {
                $model->lustrate($candidateId, $methodId);
                // Lets open post window
                $candidate = Candidates::model()->with('lustrationCount')->findByPk($candidateId);
                if($candidate !== null){
                    $url = Yii::app()->createAbsoluteUrl('/candidates/', array('view' => $candidate->id));

                    $userModel = Users::model()->findByAttributes(array('id' => Yii::app()->user->id));
                    if ($userModel->social == 'facebook') {
                        Yii::app()->user->setFlash('share-lustration', "fbshare('".$url."', '".addslashes($candidate->fullName)."', ".$candidate->getCandidateLustrations().", '".addslashes($candidate->getPunishment())."');");
                    } elseif ($userModel->social == 'vkontakte') {
                        Yii::app()->user->setFlash('share-lustration', "vkshare('".$url."', '".addslashes($candidate->fullName)."', ".$candidate->getCandidateLustrations().", '".addslashes($candidate->getPunishment())."');");
                    }
                }
            } catch (Exception $e) {
                //echo $e->getMessage();
            }
            unset(Yii::app()->request->cookies['lustration_candidate']);
            unset(Yii::app()->request->cookies['lustration_method']);
        }
        return true;
    }
    protected function postFilter($filterChain)
    {

    }
}