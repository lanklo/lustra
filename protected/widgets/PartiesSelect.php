<?php

class PartiesSelect extends CWidget
{
    public $categories;
    public $view = 'category-select';
    
    public function run()
    {
        Yii::import('application.modules.parties.models.*');
        
        $this->categories = CHtml::listData(Parties::model()->published()->findAll(array('order'=>'title')), 'id', 'title');

        $selected = Yii::app()->request->getQuery('party_id');
        $name = isset($this->categories[$selected]) ? $this->categories[$selected] : 'Партiя';
        
        $this->render($this->view, array(
            'categories' => $this->categories,
            'name' => $name,
            'selected' => $selected,
        ));
    }
}
