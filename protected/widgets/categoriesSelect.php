<?php

class categoriesSelect extends CWidget
{
    public $categories;
    public $view = 'category-select';
    
    public function run()
    {
        if(!$this->categories) return;

        $selected = Yii::app()->request->getQuery('category_id');
        $name = isset($this->categories[$selected]) ? $this->categories[$selected] : 'Партiя';
        
        $this->render($this->view, array(
            'categories' => $this->categories,
            'name' => $name,
            'selected' => $selected,
        ));
    }
}
