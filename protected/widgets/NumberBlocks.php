<?php
class NumberBlocks extends CWidget
{
    public $flag = false;
    public $number;
    private $digitCount = 5;

    public function run()
    {
        $number = str_replace('.','',$this->number);
        $price=strrev($number);
        $count = strlen($number);
        $newPrice=array();
        if($count > 5) $this->digitCount = $count;
        for($i = 0; $i < $this->digitCount; $i++){
            $class = '';
            if($i == 0) $class = ' first';
            elseif($i == ($this->digitCount - 1)) $class = ' last';
            if(isset($price[$i])) $newPrice[]='<div class="item' . $class . '">'.$price[$i].'</div>';
            else $newPrice[]='<div class="item' . $class . '">0</div>';
        }
        $html = implode('',array_reverse($newPrice));

        $this->render('numberBlocks', array('html' => $html));
    }

}
