<?php

class DateFilter extends CActiveForm
{
    public $model;
    public $name;

    public function run()
    {
        $range = $this->_getDateRange();
        $this->_getView($range);
    }

    protected function _getDateRange()
    {
        $name = $this->name;
        $range = array();
        
        $c = new CDbCriteria;
        $c->select = 'MAX(' . $name . ') as ' . $name;
        $max = $this->model->find($c);
        $range['max'] = $max;
        
        $c->select = 'MIN(' . $name . ') as ' . $name;
        $min = $this->model->find($c);
        $range['min'] = $min;
        return $range;
    }

    protected function _getView($range)
    {
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->_getField($range['min'], 'min_date');
        $this->_getField($range['max'], 'max_date');
    }

    protected function _getField($item, $name)
    {
        $param = $this->name;
//        $this->widget('CJuiDateTimePicker',array(
//            'model' => $item,
//            'attribute' => $this->name,
//            'name' => $name,
//            'mode' => 'datetime', //use "time","date" or "datetime" (default)
//            'language' => 'ru',
//            'value' => $item->$param,
//            'options' => array('dateFormat' => 'yy-mm-dd', 'timeFormat' => 'hh:mm')
//        ));
        echo "<input type='text' name='$name' value='" . (string)$item->$param . "'/>";
    }
}