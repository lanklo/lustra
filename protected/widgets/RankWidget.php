<?php
class RankWidget extends CWidget
{
    public $title;
    public $rate;
    public $allCount;
    public $class;
    public $index;
    public $maxCount = 1;
    public $col = 6;
    protected $_persantage;

    public function run()
    {
        if(!$this->allCount) $this->allCount = 1;
        if($this->maxCount === 0){
            $this->_persantage = 0;
        }else{
            $this->_persantage = round(100 * $this->rate / $this->maxCount);

            if($this->index === 0 || $this->rate == $this->maxCount)
                $this->_persantage = 100;
        }

        if($this->class == 'black')
            $this->render('rank-black');
        else
            $this->render('rank');
    }

}
