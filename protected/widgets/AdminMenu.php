<?php

class AdminMenu extends CWidget
{
    public static $menu = array(
        array('label'=>''),
        array('label'=>'Люстрации'),
        array('label'=>'Заявки на люстрацію', 'url'=>'/administration/default/requests'),
        array('label'=>'Кандидати', 'url'=>'/administration/default/admin'),
        array('label'=>'Люстрации', 'url'=>'/default/adminLustrations/admin'),
        array('label'=>'Довідники'),
        array('label'=>'Категорії', 'url'=>'/default/AdminCategories/admin'),
        array('label'=>'Типи люстрації', 'url'=>'/default/adminMethods/admin'),
        array('label'=>'Налаштування', 'url'=>'/administration/default/settings'),
        array('label'=>'Користувачi', 'url'=>'/default/adminUsers/admin'),
        array('label'=>'Партії'),
        array('label'=>'Список партій', 'url'=>'/parties/adminParties/admin'),
        array('label'=>'Синхронизация', 'url'=>'/parties/adminParties/sinhronize'),
//        array('label'=>'Члени партії', 'url'=>'#'),
    );

    public function run()
    {
        foreach(AdminMenu::$menu as $key => $item){
            
            if(isset($item['controller']) && $item['controller'] == Yii::app()->controller->id){
                if(isset($item['action']) && $item['module'] == Yii::app()->controller->module->id){
                    if($item['action'] == Yii::app()->controller->action->id)
                        $item['active'] = true;
                    else $item['active'] = false;
                }else{
                    if(isset($item['module']) && $item['module'] == Yii::app()->controller->module->id || !isset($item['module'])){
                        $item['active'] = true;
                    }
                    if($item['controller'] == 'categories'){
                        if(isset($_GET['category'])){
                            $item['active'] = false;
                            if($item['cat'] == 'sub') $item['active'] = true;
                        }                    
                        if(!isset($_GET['category'])){
                            $item['active'] = false;
                            if($item['cat'] == 'top') $item['active'] = true;
                        }
                    }    
                }
                AdminMenu::$menu[$key] = $item;
            }
        }
        $this->render('adminMenu', array('menu' => AdminMenu::$menu));
    }

}