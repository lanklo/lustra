<?php
/**
 * @autor: alex
 * @date: 18.04.12 18:48
 */

class Pager extends CLinkPager
{

    public function run()
    {
        $buttons=$this->createPageButtons();
        if(count($buttons) > 0)
            echo '<div class="pager"><ul>'.implode("",$buttons).'</ul></div>';
    }

    protected function createPageButtons()
    {
            if(($pageCount=$this->getPageCount())<=1)
                    return array();

            list($beginPage,$endPage)=$this->getPageRange();
            $currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
            $buttons=array();

            // first page
//            $buttons[]=$this->createPageButton($this->firstPageLabel,0,self::CSS_FIRST_PAGE,$currentPage<=0,false);

            // prev page
            if(($page=$currentPage-1)<0)
                    $page=0;
            $buttons[]=$this->createPageButton($this->prevPageLabel,$page,"pager_prev",$currentPage<=0,false);

            // internal pages
            for($i=$beginPage;$i<=$endPage;++$i)
                    $buttons[]=$this->createPageButton($i+1,$i,'',false,$i==$currentPage);

            // next page
            if(($page=$currentPage+1)>=$pageCount-1)
                    $page=$pageCount-1;
            $buttons[]=$this->createPageButton($this->nextPageLabel,$page,'pager_next',$currentPage>=$pageCount-1,false);

            // last page
//            $buttons[]=$this->createPageButton($this->lastPageLabel,$pageCount-1,self::CSS_LAST_PAGE,$currentPage>=$pageCount-1,false);

            return $buttons;
    }
    
    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
//            if($hidden || $selected)
//                    $class.=' '.($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
            if($selected){
                $page+=1;
                return '<li>'.CHtml::link($label,$this->createPageUrl($page),array('class' => 'active')).'</li>';
            }
            return '<li>'.CHtml::link($label,$this->createPageUrl($page),array('class' => $class)).'</li>';
    }
    
    protected function getPageRange()
    {
            $currentPage=$this->getCurrentPage();
            $pageCount=$this->getPageCount();

            $beginPage=max(0, $currentPage-(int)($this->maxButtonCount/2));
            if(($endPage=$beginPage+$this->maxButtonCount-1)>=$pageCount)
            {
                    $endPage=$pageCount-1;
                    $beginPage=max(0,$endPage-$this->maxButtonCount+1);
            }
            return array($beginPage,$endPage);
    }
    
    
    
    
    protected function createPageButtons_orig()
    {
        if(($pageCount=$this->getPageCount())<=1)
            return array();

        list($beginPage,$endPage)=$this->getPageRange();
        $currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons=array();

        // internal pages
        for($i=$beginPage;$i<=$endPage;++$i)
            $buttons[]=$this->createPageButton($i+1,$i,self::CSS_INTERNAL_PAGE,false,$i==$currentPage);

        return $buttons;
    }

    protected function createPageButton_orig($label,$page,$class,$hidden,$selected)
    {
        if($hidden || $selected){
            $page+=1;
            return "<span>{$page}</span>";
        }
        return CHtml::link($label,$this->createPageUrl($page));
    }
}