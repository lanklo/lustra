<?php
class MenuWidget extends CWidget
{
    public $items = array();
    private $number = 1;

    public function run()
    {
        $menuItems = $this->_addItems($this->items);
        $this->items = $menuItems['items'];
        $this->render('mainMenu', array('items' => $this->items));
    }

    private function _addItems($items)
    {
    	$expand = false;
    	foreach ($items as &$item) {
    		if (!empty($item['url']))
    			$item['url'] = CHtml::normalizeUrl($item['url']);
    		if (!empty($item['items'])) {
    			$menuItems = $this->_addItems($item['items']);
    			$item['items'] = $menuItems['items'];
    			$item['label'] .= '<span class="caret pull-right"></span>';
    			$item['linkOptions'] = array('class' => 'dropdown-toggle collapsed', 'data-toggle' => 'collapse',
    				'data-parent' => '#menu');
    			$item['url'] = '#collapse'.$this->number;
    			$item['submenuOptions'] = array('class' =>
    				'nav nav-list collapse'.($menuItems['expand'] ? ' in' : ''),
    				'id' => 'collapse'.$this->number);
    			$this->number++;
    		}
//    		if ($item['url']) {
//    			$urlParts = explode('/', $item['url']);
//    			if (@$urlParts[1] == $this->controller->module->id  &&
//    			    @$urlParts[2] == $this->controller->id) {
//    			    $item['active'] = true;  $expand = true;
//    			    if (@$urlParts[3] == $this->controller->action->id)
//    			    	$item['url'] = '';
//    			}
//    		}
    	}
        return array('items' => $items, 'expand' => $expand);
    }
}
