<?php

Yii::import('bootstrap.widgets.TbActiveForm');

class AdminFormWidget extends TbActiveForm
{

    public function imageFieldRow($model, $attribute, $htmlOptions = array())
    {
        if(!isset($htmlOptions['accept'])) $htmlOptions['accept'] = 'image/*';
        
        $input = $this->inputRow(TbInput::TYPE_FILE, $model, $attribute, null, $htmlOptions);

        if(!empty($model->$attribute)){
            $input .= "<div class='control-group'><div class='controls'>";
            $input .= CHtml::image($model->imageUrl);
            if(!$model->isAttributeRequired($attribute)){
                $input .= '<label class="checkbox">
                        <input name="'.get_class($model).'[delete]['.$attribute.']" id="Img_delete" value="1" type="checkbox">
                        Удалить файл
                    </label>';
            }
            $input .= '</div></div>';
        }
        
        return $input;
    }
}
