<?php

class FormWidget extends CActiveForm
{

    public function textFieldRow($model, $attribute, $htmlOptions = array())
    {
        if(!isset($htmlOptions['placeholder']))
            $htmlOptions['placeholder'] = $this->_getLabel($model, $attribute);
        
        $input = "<label class='$attribute'>";
        $input .= $this->textField($model, $attribute, $htmlOptions);
        if(!$model->isAttributeRequired($attribute))
            $input .= '<span class="empty-message">*Це поле обов\'язкове.</span>';
        $input .= '</label>';
        
        return $input;
    }

    public function textAreaFieldRow($model, $attribute, $htmlOptions = array())
    {
        if(!isset($htmlOptions['placeholder']))
            $htmlOptions['placeholder'] = $this->_getLabel($model, $attribute);

        $input = "<label class='$attribute'>";
        $input .= $this->textArea($model, $attribute, $htmlOptions);
        if(!$model->isAttributeRequired($attribute))
            $input .= '<span class="empty-message">*Це поле обов\'язкове.</span>';
        $input .= '</label>';

        return $input;
    }

    protected function _getLabel($model, $attribute)
    {
        $label = $model->getAttributeLabel($attribute);
        if($model->isAttributeRequired($attribute))
            $label .= '*';
        $label .= ':';
        return $label;
    }
}
