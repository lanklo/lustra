<div class="rating <?= $this->class ?>">
    <div class="rate col-lg-<?= $this->col ?> text-left">
        <div class="bar" style="width: <?= $this->_persantage ?>% ">
            <?php if($this->title): ?><div class="bar-title"><?= $this->title ?></div><?php endif; ?>
        </div>
        <div class="text-black-bkg"><?= $this->title ?></div>
    </div>
    <div class="col-lg-1 text-left"><?= $this->rate ?></div>
    <div class="clearfix"></div>
</div>