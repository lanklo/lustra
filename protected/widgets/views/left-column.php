<h3><span class="fa fa-star"></span>Категорії кандидатів</h3>
<ul class="list2">
    <li>
        <?= CHtml::link("ВСІ", array('/candidates')) ?>
    </li>
    <?php foreach($categories as $k => $item): ?>
        <li>
        <?= CHtml::link($item, array('/candidates', 'category_id' => $k), array(
                'class' => ($selected == $k) ? 'active' : ''
        )) ?>
        </li>
    <?php endforeach; ?>
</ul>