<?php if(!empty($items)): ?>
    <nav class="navbar navbar-default navbar-static-top tm_navbar clearfix" role="navigation">
    <?php $this->widget('zii.widgets.CMenu', array(
          'items' => $items,
          'encodeLabel' => false,
          'htmlOptions' => array('class' => 'nav sf-menu clearfix')
     )); ?>
    </nav>
<?php endif; ?>
