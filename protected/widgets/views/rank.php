<div class="rating <?= $this->class ?>">
    <?php if($this->title && $this->class != 'black'): ?>
    <div class="col-lg-4 text-left"><?= $this->title ?></div>
    <?php endif; ?>
    <div class="rate col-lg-<?= $this->col ?> text-left">
        <div class="bar" style="width: <?= $this->_persantage ?>% ">
            <?php if($this->title && $this->class == 'black'): ?>
                <?= $this->title ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>