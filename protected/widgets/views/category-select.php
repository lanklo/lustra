<div class="btn-group head_list">
<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <?= $name ?>
    <span class="caret"></span>
</button>
<ul class="dropdown-menu">
    <?php foreach($categories as $k => $item): ?>
        <li>
            <?= CHtml::link($item, "/candidates/party_id/$k", array(
                'class' => ($selected == $k) ? 'active' : ''
            )) ?>
        </li>
    <?php endforeach; ?>
</ul>
</div>