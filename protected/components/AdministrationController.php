<?php

/**
 * Класс авторизации и инициализации в админ-панель
 * 
 */
class AdministrationController extends MainAppController
{

    public $layout = '//layouts/administration';

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);

        // проверка авторизации
        $this->checkAuth();

        // инициализация глобальных параметров
        $this->init();
    }
    
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('admin', 'author'),
            ),
            array('deny',
                    'users'=>array('*'),
                ),
        );
    }

    public function init()
    {
        parent::init();
        Yii::app()->getComponent('bootstrap');
        Yii::app()->clientScript->registerScriptFile('/js/script.js');
//        Yii::app()->clientScript->registerScriptFile('/js/admin/tiny_mce/tiny_mce.js');
//        Yii::app()->clientScript->registerScriptFile('/js/admin/tiny_mce/tiny_initialization.js');
    }

    public function checkAuth()
    {
        if (Yii::app()->user->isGuest)
        {
            $this->forward("/default/login");
        }
    }

}