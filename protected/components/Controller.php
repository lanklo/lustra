<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
//	public $layout='//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $categories = array();

    protected function beforeAction($action)
    {
        parent::beforeAction($action);

        $this->categories = CHtml::listData(Categories::model()->findAll(array('order'=>'id')), 'id', 'title');
        $this->_addIphoneIcons();
        $this->_addCss();
        $this->_addJs();

        return true;
    }

    protected function _addCss()
    {
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap.css');
        $cs->registerCssFile(Yii::app()->baseUrl . '/css/style.css');
        $cs->registerCssFile(Yii::app()->baseUrl . '/fonts/font-awesome.css');
    }

    private function _addJs()
    {
        $cs = Yii::app()->clientScript;
        $cs->defaultScriptFilePosition = CClientScript::POS_END;
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery-1.7.1.min.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery-migrate-1.2.1.min.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.easing.1.3.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.mobilemenu.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.mousewheel.min.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.mobilemenu.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.touchSwipe.min.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js');
    }

    private function _addIphoneIcons()
    {
        $cs = Yii::app()->clientScript;
        $cs->registerLinkTag('apple-touch-icon-precomposed', null,
                Yii::app()->baseUrl . '/images/apple-touch-icon-144x144-precomposed.png', null,
                array('sizes' => '144x144'));
        $cs->registerLinkTag('apple-touch-icon-precomposed', null, 
                Yii::app()->baseUrl . '/images/apple-touch-icon-114x114-precomposed.png', null,
                array('sizes' => '114x114'));
        $cs->registerLinkTag('apple-touch-icon-precomposed', null,
                Yii::app()->baseUrl . '/images/apple-touch-icon-72x72-precomposed.png', null,
                array('sizes' => '72x72'));
        $cs->registerLinkTag('apple-touch-icon-precomposed', null,
                Yii::app()->baseUrl . '/images/apple-touch-icon-57x57-precomposed.png');
    }
}
