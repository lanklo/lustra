<?php

/**
 * @property string $savePath путь к директории, в которой сохраняем файлы
 */
class UploadableFileResizeBehavior extends CActiveRecordBehavior{
    /**
     * @var string название атрибута, хранящего в себе имя файла и файл
     */
    public $attributeName = 'img';
    /**
     * @var string алиас директории, куда будем сохранять файлы
     */
    public $savePathAlias = 'webroot.uploaded';
    /**
     * @var array сценарии валидации к которым будут добавлены правила валидации
     * загрузки файлов
     */
    public $scenarios = array('insert', 'update');
    /**
     * @var string типы файлов, которые можно загружать (нужно для валидации)
     */
    public $fileTypes = 'gif, jpeg, jpg, png';

    public $width = 300;
    public $height = 300;

    public $maxSize = 4194304; //4 Mb

    /**
     * Шорткат для Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR.
     * Возвращает путь к директории, в которой будут сохраняться файлы.
     * @return string путь к директории, в которой сохраняем файлы
     */
    public function getSavePath()
    {
        return Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR;
    }

    public function getSaveThumbPath()
    {
        return Yii::getPathOfAlias($this->savePathAlias . '.thumb.').DIRECTORY_SEPARATOR;
    }

    public function attach($owner){
        parent::attach($owner);

        if(in_array($owner->scenario,$this->scenarios)){
            // добавляем валидатор файла
            $fileValidator = CValidator::createValidator('file', $owner,
                $this->attributeName,
                array(
                    'types' => $this->fileTypes,
                    'maxSize' => $this->maxSize,
                    'allowEmpty' => true
            ));
            $owner->validatorList->add($fileValidator);
        }
        return true;
    }
    
    public function afterValidate($event)
    {
        if(in_array($this->owner->scenario,$this->scenarios) &&
            (!($file = EUploadedImage::getInstance($this->owner, $this->attributeName)) || !$file->name)){
//            $this->owner->addError($this->attributeName, 
//                "Необходимо заполнить поле {$this->owner->getAttributeLabel($this->attributeName)}.");
        }
    }

    // имейте ввиду, что методы-обработчики событий в поведениях должны иметь
    // public-доступ начиная с 1.1.13RC
    public function beforeSave($event)
    {
        if(in_array($this->owner->scenario,$this->scenarios) &&
            ($file = EUploadedImage::getInstance($this->owner, $this->attributeName)) && $file->name){
            $this->deleteFile();
            $name = rand() . time() . preg_replace('/(^.*)(\.)/', '$2', $file->name);
            $this->owner->setAttribute($this->attributeName, $name);
            $file->saveAs($this->savePath.$name);

            if($this->height && $this->width){
                $file->reset();
                $file->maxWidth = $this->width;
                $file->maxHeight = $this->height;

                $file->saveAs($this->saveThumbPath.$name);

//                if($file->getWidth() > $file->getHeight())
//                    $file->maxWidth = $this->width;
//                else $file->maxHeight = $this->height;
                
//                $file->saveAs($this->saveThumbPath.$name);
            }
        }
        return true;
    }

    // имейте ввиду, что методы-обработчики событий в поведениях должны иметь
    // public-доступ начиная с 1.1.13RC
    public function beforeDelete($event)
    {
        $this->deleteFile(); // удалили модель? удаляем и файл, связанный с ней
    }

    public function deleteFile()
    {
        $filePath=$this->savePath.$this->owner->getAttribute($this->attributeName);
        if(@is_file($filePath))
            @unlink($filePath);
        
        $filePath=$this->saveThumbPath.$this->owner->getAttribute($this->attributeName);
        if(@is_file($filePath))
            @unlink($filePath);
    }
}
