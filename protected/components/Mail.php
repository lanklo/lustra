<?php

Yii::import('application.vendors.phpmailer.*');

class Mail
{

    /**
     * @var PHPMailer 
     */
    private $_mailer;
    private $_from;
    private $_fromName;

    public function __construct()
    {
        $this->_mailer = new PHPMailer();
        $this->_mailer->IsHTML(true);
        $this->_mailer->CharSet = 'UTF-8';
        $this->_from = Yii::app()->params['fromEmail'];
        $this->_fromName = Yii::app()->params['systemSenderName'];
    }
    
    protected static function _sendMail($subject, $body, $mail = false)
    {
        $mailer = new PHPMailer();
        $mailer->CharSet = 'UTF-8';
        $mailer->From = Yii::app()->params['fromEmail'];
        $mailer->FromName = Yii::app()->params['fromName'];
        
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        if($mail)
            $mailer->AddAddress($mail);
        else
            $mailer->AddAddress(Yii::app()->params->toEmail);
//        $mailer->AddAddress('kristinaurbas1@gmail.com');

        if(!$mailer->Send()) return false;
        return true;
    }

    public static function sendMail($subject, $body, $mail = false)
    {
        $subject = Yii::app()->name . '. ' . $subject;
        
        return self::_sendMail($subject, $body, $mail);
    }
}
