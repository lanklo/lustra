<?php

class UserIdentity extends CUserIdentity
{

    private $_id;

    private $_errorMessage = array();

    public function __construct(array $params)
    {
        $this->_errorMessage = array(
            1 => Yii::t('global', 'Неизвестный логин'),
            2 => Yii::t('global', 'Неверный пароль')
        );
        
        $this->username = $params['user_name'];
        $this->password = $params['password'];
    }


    public function authenticate()
    {
        $record = $this->_getUserData();
        if(!$record)
        {
            return false;
        }
        else
        {
            $this->_id = $record->id;
            $this->setState('login', $record->login);
            
            //$this->errorCode = self::ERROR_NONE;
        }
        return true;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getError()
    {
        return $this->_errorMessage[$this->errorCode];
    }

    private function _getUserData()
    {
        $record = Users::model()->findByAttributes(array('login' => $this->username));
        if ($record === null){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            return false;
        }
        else if ($record->password !== md5($record->salt . md5($this->password))){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            return false;
        }

        return $record;
    }

}
