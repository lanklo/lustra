<?php

class AdminMenu extends CWidget
{
    public static $menu = array(
        array(
            'label' => 'Блоги'),
        array(
            'label' => 'Авторы',
            'url' => '/administration/blog/index',
            'controller' => 'admin',
            'module' => 'blog'),
        array(
            'label' => 'Посты',
            'url' => '/administration/blog_posts/index',
            'controller' => 'adminPosts',
            'module' => 'blog'),
        array(
            'label' => 'Контент'),
        array(
            'label' => 'Статические страницы',
            'url' => '/administration/statpage/index',
            'controller' => 'admin',
            'module' => 'statpage'),
        array(
            'label' => 'Афиша',
            'url' => '/administration/events/index',
            'controller' => 'admin',
            'module' => 'events'),
        array(
            'label' => 'Интересно для мам',
            'url' => '/administration/articles/index',
            'controller' => 'admin',
            'module' => 'articles'),
        array(
            'label' => 'Баннеры для слайдера',
            'url' => '/administration/banners/index',
            'controller' => 'admin',
            'module' => 'banners',
            'action' => 'index'),
        array(
            'label' => 'Баннеры',
            'url' => '/administration/banners/bottom',
            'controller' => 'admin',
            'module' => 'banners',
            'action' => 'bottom'),
        array(
            'label' => 'Справочники'),
        array(
            'label' => 'Категории',
            'url' => '/administration/categories/admin',
            'controller' => 'categories',
            'cat' => 'top'),
        array(
            'label' => 'Подкатегории',
            'url' => '/administration/categories/admin?category=sub',
            'controller' => 'categories',
            'cat' => 'sub'),
        array(
            'label' => 'Темы публикаций',
            'url' => '/administration/blog_subjects/index',
            'controller' => 'adminSubjects',
            'module' => 'blog'),
        array(
            'label' => 'Партнеры'),
        array(
            'label' => 'Список',
            'url' => '/administration/partners/index',
            'module' => 'partners',
            'controller' => 'admin'),
        array(
            'label' => 'Клиенты',
            'url' => '/administration/clients/index',
            'module' => 'clients',
            'controller' => 'admin'),
        array(
            'label' => 'Подписчики',
            'url' => '/administration/subscribers/index',
            'module' => 'subscribers',
            'controller' => 'admin'),
        array(
            'label' => 'Конкурсы'
        ),
        array(
            'label' => 'Конкурсы',
            'url' => '/administration/konkurs/index',
            'module' => 'konkurs',
            'controller' => 'admin'
        ),
        array(
            'label' => 'Материалы',
            'url' => '/administration/materials/index',
            'module' => 'konkurs',
            'controller' => 'adminMaterials'
        )
    );

    public function run()
    {
        foreach(AdminMenu::$menu as $key => $item){
            
            if(isset($item['controller']) && $item['controller'] == Yii::app()->controller->id){
                if(isset($item['action']) && $item['module'] == Yii::app()->controller->module->id){
                    if($item['action'] == Yii::app()->controller->action->id)
                        $item['active'] = true;
                    else $item['active'] = false;
                }else{
                    if(isset($item['module']) && $item['module'] == Yii::app()->controller->module->id || !isset($item['module'])){
                        $item['active'] = true;
                    }
                    if($item['controller'] == 'categories'){
                        if(isset($_GET['category'])){
                            $item['active'] = false;
                            if($item['cat'] == 'sub') $item['active'] = true;
                        }                    
                        if(!isset($_GET['category'])){
                            $item['active'] = false;
                            if($item['cat'] == 'top') $item['active'] = true;
                        }
                    }    
                }
                AdminMenu::$menu[$key] = $item;
            }
        }
        $this->render('adminMenu', array('menu' => AdminMenu::$menu));
    }

}