<?php

/**
 * Главный контроллер задающий дополнительные свойства и методы
 * для всех приложений
 */
class MainAppController extends CController
{

    private $dataAssignVars = array();

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);

    }

    public function render($view, $data = array(), $return = false)
    {
        if (! is_array($data))
        {
            $data = array($data);
        }
        return parent::render($view, array_merge($data, $this->dataAssignVars), $return);
    }

    public function assign($key, $value = '')
    {
        $this->dataAssignVars[$key] = $value;
    }

}
