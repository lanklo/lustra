<?php

Yii::import('application.models.*');

class WebUser extends CWebUser
{

    private $_role = null;
    public $loginUrl = null;
    public $social = null;
    public $email = null;

    public function getRole()
    {
        if ($role = $this->getModel())
        {
            // в таблице User есть поле role
            return $role;
        }
    }

    private function getModel()
    {
        if ( ! $this->isGuest && $this->_role === null)
        {
            $this->_role = Users::getUserRole($this->id);
        }
        return $this->_role;
    }

}
