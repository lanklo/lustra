<div class="btm-block">
    <?php if(Yii::app()->user->isGuest || !$data->lustrated): ?>
        <div class="text-left methods">
            <?php foreach($data->methodStatistic as $item): ?>
                <label>
                    <div class="percentage"<?php if ($item->title == $data->punishment): ?> style="font-weight: bold;"<?php endif ?>><?= $item->persentage ?>%</div>
                    <input type="radio" name="Lustration[method_id][<?= $data->id ?>]" value="<?= $item->id ?>"/>
                    <span></span>
                    <?= $item->title ?>
                </label><br/>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
        <div class="text-left methods lustratiion-result">
            <?php foreach($data->methodStatistic as $item): ?>
                <label>
                    <?= $item->title ?> <span class="pers"<?php if ($item->title == $data->punishment): ?> style="font-weight: bold;"<?php endif ?>><?= $item->persentage ?>%</span>
                </label><br/>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    
    <?php if(Yii::app()->user->isGuest): ?>
        <?= CHtml::link('Я люструю!', 'javascript:void(0)', array(
            'class' => 'lustruu disabled',
//            'disabled' => 'disabled',
            'onclick' => "saveLustration($data->id)",
            'data-toggle' => 'modal',
            //'data-target' => '#popUp',
        )) ?>
    <?php else: ?>
        <?php if(!$data->lustrated): ?>
            <?= CHtml::link('Я люструю!', 'javascript:void(0)', array(
                'class' => 'lustruu disabled',
//                'disabled' => 'disabled',
                'onclick' => "lustration($data->id)",
            )) ?>
        <?php endif; ?>
    <?php endif; ?>
</div>