<article class="col-lg-3 col-md-3 col-sm-3">
    <form id="search" class="search" action="<?= Yii::app()->baseUrl ?>/candidates/" method="GET" accept-charset="utf-8">
         <input type="text" name="search_string" value="Пошук" onfocus="if (this.value == 'Пошук') {this.value=''}" onblur="if (this.value == '') {this.value='Пошук'}">
         <a href="#" onClick="document.getElementById('search').submit()"><img src="<?= Yii::app()->baseUrl ?>/img/magnify.png" alt=""></a>
    </form>
    <h3><span class="fa fa-user"></span>Лiчильник кандидатів</h3>
    <?php $this->widget('NumberBlocks', array(
        'number' => Candidates::model()->count(),
    )); ?>
    <?php $this->widget('categoriesSelect', array(
        'categories' => $this->categories,
        'view' => 'left-column'
    )); ?>
</article>
<div class="clearfix"></div>