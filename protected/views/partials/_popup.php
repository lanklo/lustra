<div class="modal fade" id="popUp" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <?php if(Yii::app()->user->isGuest): ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">
                Залогіньтесь від імені профілю соцмережі
            </h4>
        </div>
        <div class="modal-body">
            <?php $this->widget('ext.eauth.EAuthWidget', array(
                'action' => '/login',
                'popup' => true
                )); ?>
            або
            <? /*= CHtml::link('Інкогніто', 'javascript:void(0)', array(
                'class' => 'btn-default btn1',
                'data-toggle' => 'modal',
                'data-target' => '#anonim',
                'style' => 'margin: 0 0 0 5px'
            )) */?>
            <?= CHtml::link('Інкогніто', 'javascript:void(0)', array(
            'class' => 'btn-default btn1',
            'onclick' => 'anonimAuthorize()',
            'style' => 'margin: 0 0 0 5px'
        )) ?>
        </div>
        <?php else: ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="return emailConfirmClose();">&times;</button>
            <h4 class="modal-title" id="myModalLabel">
                Залиште свій Email і ми повідомимо вас, якщо ця, або будь-яка інша люструвана вами людина, спробує потрапити у владу.
            </h4>
          </div>
          <div class="modal-body">
                <?php $form = $this->beginWidget('FormWidget', array(
                        'id'=>'contact-form',
                )); ?>
                <?= $form->errorSummary($user); ?>
                <div class="form-div-1 clearfix">
                    <?= $form->textFieldRow($user, 'first_name', array(
                        'style' => 'border:1px solid #cccccc'
                    )) ?>
                </div>
                <div class="clearfix"></div><br/>
                <div class="form-div-1 clearfix">
                    <?= $form->textFieldRow($user, 'email', array(
                        'style' => 'border:1px solid #cccccc'
                    )) ?>
                </div>
                <div class="clearfix"></div>
                <div class="btns">
                    <input type="submit" value="Підтвердити" onClick="emailConfirm();" class="btn-default btn1"/>
                    <a href="javascript:void(0);" style="line-height: 42px; padding:20px;" onClick="emailConfirmClose(); $('#popUp').modal('hide');">Відміна</a>
                </div>
                <?php $this->endWidget(); ?>
          </div>
        <?php endif; ?>
    </div>
  </div>
</div>
<?php $this->renderPartial('//partials/_method_modal') ?>
<?php $this->renderPartial('//partials/_anonim', compact('user')); ?>
<?php $this->renderPartial('//partials/_email_form', compact('user')); ?>