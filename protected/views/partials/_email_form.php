<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">
            Залиште свій Email і ми повідомимо вас, якщо ця, або будь-яка інша люструвана вами людина, спробує потрапити у владу.
        </h4>
      </div>
      <div class="modal-body">
            <?php $form = $this->beginWidget('FormWidget', array(
                    'id'=>'contact-form',
            )); ?>
            <?= $form->errorSummary($user); ?>
            <div class="form-div-1 clearfix">
                <?= $form->textFieldRow($user, 'email', array(
                    'style' => 'border:1px solid #cccccc'
                )) ?>
            </div>
            <div class="form-div-1 clearfix">
                <?= $form->textField($user, 'first_name', array(
                    'style' => 'border:1px solid #cccccc'
                )) ?>
            </div>
            <div class="clearfix"></div>
            <div class="btns">
                <input type="submit" value="Підтвердити" class="btn-default btn1"/>
            </div>
            <?php $this->endWidget(); ?>
      </div>
    </div>
  </div>
</div>