<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="<?= Yii::app()->baseUrl ?>/img/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="<?= Yii::app()->baseUrl ?>/img/favicon.ico" type="image/x-icon" />
        <meta name = "format-detection" content = "telephone=no" />
        
        <!--[if lt IE 9]>
        <div style='text-align:center'>
            <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
                <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
            </a>
        </div>
        <![endif]-->
	<title><?= CHtml::encode($this->pageTitle) ?> | <?= Yii::app()->name ?></title>
        <?= $this->renderPartial('//partials/_ga') ?>
</head>
<body>
    <?= $this->renderPartial('//partials/_jsdk') ?>
    <div class="container">
        <div class="row">
            <article class="col-lg-12 col-md-12 col-sm-12">
                <header class="clearfix">
                    <h1 class="navbar-brand navbar-brand_">
                        <?= CHtml::link('<img src="/img/logo.png" alt="">', '/') ?>
                    </h1>

                    <?= CHtml::link('Додати кандидата', '/candidates/add', array('class' => 'red-link')) ?>

                    <div class="menuBox clearfix">
                        <?php $this->widget('MenuWidget', array(
                            'items' => array(
                                array('label' => 'Головна', 'url' => '/'),
                                array('label' => 'Партії', 'url' => '/parties/'),
                                array('label' => 'Зворотній зв\'язок', 'url' => array('/contacts/')),
                            )
                        )); ?>
                    </div>

                    <?php if (!Yii::app()->user->isGuest) : ?>
                        <?= CHtml::link('Вийти', '/site/logout', array('class' => 'red-link')) ?>
                    <?php endif ?>

                <?php $this->widget('PartiesSelect'); ?>

                </header>
            </article>
        </div>
    </div>
    <div class="global">
        <div class="container">
            <?= $content ?>
            <?php if(!(Yii::app()->controller->id == 'index' && Yii::app()->controller->action->id == 'index')): ?>
                <?php $this->renderPartial('//partials/left-column') ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <article class="col-lg-12 col-md-12 col-sm-12">
                <footer>
                    <p>Приєднуйся до Всеукраїнської люстрації!</p>
                </footer>
            </article>
        </div>
    </div>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</body>
</html>
