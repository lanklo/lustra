<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Admin Panel - <?php echo CHtml::encode(Yii::app()->name); ?></title>
    <script type="text/javascript" src="/js/admin/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="/js/admin/tiny_mce/tiny_initialization.js"></script>
</head>
<body>
    <?php $this->widget('bootstrap.widgets.TbNavbar', array(
        'type'=>'inverse', 
        'brand'=>CHtml::encode(Yii::app()->name),
        'brandUrl'=>'/',
        'collapse'=>true, // requires bootstrap-responsive.css
        'items'=>array(
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>array(
                    array('label'=>'Home', 'url'=>'/'),
                    array('label'=>'Administration', 'url'=>'/administration'),
                ),
            ),
            '<div class="btn-group pull-right">
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-user"></i> ' . Yii::app()->user->name . '
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="' . $this->createUrl('/default/login/exit') . '">Выйти</a></li>
                </ul>
            </div>',
        ),
    )); ?>
    <div id="wrapper">
        <?php $this->widget('AdminMenu'); ?>
        <div class="container-fluid" style="float:left; margin-top: 50px; width: 70%;">
            <div class="row-fluid">
                <div class="span12">
                    <div class="well sidebar-nav">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>