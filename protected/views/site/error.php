<?php
/* @var $this SiteController */
/* @var $error array */
?>
<article class="col-lg-9 col-md-12 col-sm-12 error-page">
    <h3 class="center rouse">Error <?php echo $code; ?></h3>
    <p class="center">
        <?php if($code == '404'): ?>
            Сторінку не знайдено
        <?php else: ?>
            <?= CHtml::encode($message); ?>
        <?php endif;?>
    </p>
</article>