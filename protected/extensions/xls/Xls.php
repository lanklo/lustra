<?php
/**
 * Date: 08.11.11
 * Time: 13:00
 * Copyright (c) 2011 Trigub Alexandr (atrigub@gmail.com)
 */

//Yii::import('application.vendors.phpExcel.*');

class Xls
{
    private $_exel;
    private $_baseFont;
    private $_boldFont;
    private $_center;
    private $_aSheet;


    public function __construct()
    {

        $phpExcelPath = Yii::getPathOfAlias('application.vendors.phpExcel');
        spl_autoload_unregister(array('YiiBase','autoload'));
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $this->_exel = new PHPExcel();
        $this->_exel->setActiveSheetIndex(0);
        $this->_aSheet = $this->_exel->getActiveSheet();
        $this->_aSheet->setTitle('Первый лист');

        $this->_baseFont = array(
            'font' => array(
                'name' => 'Arial Cyr',
                'size' => '10',
                'bold' => false
            )
        );
        $this->_boldFont = array(
            'font' => array(
                'name' => 'Arial Cyr',
                'size' => '10',
                'bold' => true
            )
        );
        $this->_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP
            )
        );
    }

    public function setHeader($data)
    {
        foreach ($data as $k => $d) {
            $this->_aSheet->setCellValue($k . '1', $d);
            $this->_aSheet->getStyle($k . '1')->applyFromArray($this->_boldFont)->applyFromArray($this->_center);
        }
    }

    public function setWidth($data)
    {
        foreach ($data as $k => $d) {
            $this->_aSheet->getColumnDimension($k)->setWidth($d);
        }
    }

    public function setHeight($data)
    {
        foreach ($data as $k => $d) {
            $this->_aSheet->getRowDimension($k)->setRowHeight($d);
        }
    }

    public function setupData($data, $utils)
    {
        //var_dump($data);die;
        $counter = 0;
        foreach ($data as $key => $val) {
            foreach ($utils as $k => $d) {
                $this->_aSheet->setCellValue($k . ($counter + 2), $val[$utils[$k]]);
                $this->_aSheet->getStyle($k . ($counter + 2))->applyFromArray($this->_baseFont);
            }
            $counter++;
        }
    }

    public function getXLS()
    {
        $objWriter = new PHPExcel_Writer_Excel5($this->_exel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="load-stat.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    public function save($path)
    {
        try {
            $objWriter = new PHPExcel_Writer_Excel5($this->_exel);
            $objWriter->save($path);
            spl_autoload_register(array('YiiBase','autoload'));
            return true;
        } catch(Exception $e){
            spl_autoload_register(array('YiiBase','autoload'));
            return false;
        }

    }

    public function readXls()
    {
        $path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . 'party-list.xlsx';
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($path);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($path,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        spl_autoload_register(array('YiiBase','autoload'));
        return $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

    }
}