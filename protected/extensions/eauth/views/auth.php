<div class="services">
	<ul class="auth-services clear">
		<?php
		foreach ($services as $name => $service) {
			echo '<li style="margin-right:10px" class="auth-service ' . $service->id . '">';
            echo '<a href="/login/service/'.$name.'?url='.urlencode(Yii::app()->request->requestUri).'" class="auth-link '.$service->id.'">';
			echo '<span class="auth-icon ' . $service->id . '"><i></i></span>';
            echo '</a>';
			echo '</li>';
		}
		?>
	</ul>
</div>