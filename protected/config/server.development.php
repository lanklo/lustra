<?php

return array(
    'connectionString' => 'mysql:host=localhost;dbname=lustralu_lustration',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    // включить кэширование схем для улучшения производительности
    'schemaCachingDuration' => 0,
    // включаем профайлер
    'enableProfiling' => true,
    // показываем значения параметров
    'enableParamLogging' => true,
);
