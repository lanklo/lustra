<?php
if ( ! isset($environment))
{
//    $environment = "production";
    $environment = $_SERVER['APPLICATION_ENV'];
}

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Люстрацiя',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'application.filters.*',
                'application.modules.*',
                'application.helpers.*',
                'application.extensions.*',
                'application.widgets.*',
                'application.modules.default.models.*',
                'ext.xls.Xls',
                'ext.eoauth.*',
                'ext.eoauth.lib.*',
                'ext.lightopenid.*',
                'ext.eauth.*',
                'ext.eauth.services.*',
	),
        'defaultController'=>'default',
        'language'=>'ua',

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
                        'generatorPaths'=>array(
                            'bootstrap.gii',
                        ),
		),
                'default',
                'parties',
	),
	'components'=>array(
                'language' => 'ua',
                'coreMessages' => array(
                    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'messages'
                ),
		'user'=>array(
                        'class' => 'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'autoUpdateFlash' => false,
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName' => false,
                        'rules' => require(dirname(__FILE__).'/rules.php'),
		),
		
                'db' => require(dirname(__FILE__) . '/server.' . $environment . '.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
            
                'bootstrap'=>array(
                    'class'=>'ext.bootstrap.components.Bootstrap',
                ),

                'authManager'=>array(
                    'class'=>'AuthManager',
                    'defaultRoles'=>array('guest'),
                ),
            
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
                'loid' => array(
                    'class' => 'ext.lightopenid.loid',
                ),
                'cache' => array(
                    'class' => 'system.caching.CFileCache',
                ),
                'eauth' => array(
                    'class' => 'ext.eauth.EAuth',
                    'popup' => true,
//                    'cache' => false,
//                    'cacheExpire' => 0,
                    'services' => array(
                        'facebook' => array(
                            'class' => 'FacebookOAuthService',
                            'client_id' => '299284326893772',
                            'client_secret' => '9dd8cb46813e63881c14d9cc3bccba2e',
                            'scope' => 'email'
                        ),
                        'vkontakte' => array(
                            'class' => 'VKontakteOAuthService',
                            'client_id' => '4324105',
                            'client_secret' => '4oQNSfOrMKJ9jhdaReGM',
                        ),
                    ),
                ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
        'params'=>require(dirname(__FILE__).'/params.php'),
);