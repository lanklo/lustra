<?php

return array(
    'administration' => 'default/admin',
    'administration/<module:\w+>/<action:\w+>/*' => '<module>/admin/<action>',
    'candidates/view/<id>/*' => '/default/candidates/view/id/<id>',
    'candidates/add' => '/default/candidates/add',
    'contacts' => '/default/index/contacts',
    'candidates/lustra' => '/default/candidates/lustra',
    'candidates/*' => 'default/candidates',
    'candidates/category_id/<category_id>' => 'default/candidates/*',
    '/login/*' => '/default/index/login',
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
);
