<?php

return array(
    'connectionString' => 'mysql:host=localhost;dbname=lustration',
    'emulatePrepare' => true,
    'username' => 'lustration',
    'password' => 'E6L8Vm078XMd',
    'charset' => 'utf8',
    // включить кэширование схем для улучшения производительности
    'schemaCachingDuration' => 0,
    // включаем профайлер
    'enableProfiling' => true,
    // показываем значения параметров
    'enableParamLogging' => true,
);
