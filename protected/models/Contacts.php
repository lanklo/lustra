<?php

/**
 * This is the model class for table "contacts".
 *
 * The followings are the available columns in table 'contacts':
 * @property integer $id
 * @property string $create_time
 * @property string $name
 * @property string $email
 * @property string $subjects
 * @property string $text
 */
class Contacts extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contacts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, email, text', 'required'),
                        array('name, text, subjects', 'normalize'),
                        array('name, text, subjects','filter','filter'=>array(new CHtmlPurifier(),'purify')),
                        array('email', 'email'),
			array('name, email, subjects', 'length', 'max'=>255),
			array('id, create_time, name, email, subjects, text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Create Time',
			'name' => "Ваше ім'я",
			'email' => 'Email',
			'subjects' => 'Тема',
			'text' => 'Текст повідомлення',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('subjects',$this->subjects,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function afterConstruct()
        {
            parent::afterConstruct();

            if(!Yii::app()->user->isGuest){
                $this->name = Yii::app()->user->name;
                if(!Yii::app()->user->email){
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    Yii::app()->user->email = $user->email;
                }
                $this->email = Yii::app()->user->email;
            }
        }

        public function normalize($attribute)
        {
            $this->$attribute = htmlspecialchars(trim($this->$attribute));
        }
}