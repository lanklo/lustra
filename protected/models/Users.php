<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $social
 * @property string $sid
 * @property string $create_time
 * @property string $first_name
 * @property string $last_name
 * @property integer $email
 */
class Users extends CActiveRecord
{
    public $password_repeat;
    
    public function tableName()
    {
            return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('social, sid', 'required'),
                    array('password, password_repeat', 'required', 'on' => 'admin-settings'),
                    array('password', 'compare', 
                        'compareAttribute' => 'password_repeat',
                        'enableClientValidation' => false, 
                        'on' => 'admin-settings',),
//                    array('email', 'required', 'on' => 'anonim'),
                    array('email', 'email', 'except' => 'anonim'),
                    array('sid, first_name, last_name', 'length', 'max'=>255),
                    // The following rule is used by search().
                    // @todo Please remove those attributes that should not be searched.
                    array('id, social, sid, create_time, first_name, last_name, email', 'safe', 'on'=>'search'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'lustrations' => array(self::HAS_MANY, 'Users', 'user_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'social' => 'Тип',
                    'sid' => 'Sid',
                    'create_time' => 'Дата реєстрації',
                    'first_name' => "Iм'я",
                    'last_name' => 'Прізвище',
                    'email' => 'Email',
                    'password' => 'Пароль',
                    'password_repeat' => 'Повторить пароль',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('social',$this->social,true);
            $criteria->compare('sid',$this->sid,true);
            $criteria->compare('create_time',$this->create_time,true);
            $criteria->compare('first_name',$this->first_name,true);
            $criteria->compare('last_name',$this->last_name,true);
            $criteria->compare('role',$this->role);
            $criteria->compare('email',$this->email);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    public static function getUserRole($userId)
    {
        $c = Yii::app()->db->createCommand()
                ->select('user_roles.name')
                ->from('users')
                ->join('user_roles', 'user_roles.id=users.role')
                ->where("users.id='{$userId}'")
                ->queryRow();
                
        if ($c) return $c["name"];
    }

    public function afterDelete()
    {
        parent::afterDelete();

        Lustrations::model()->deleteAllByAttributes(array('user_id' => $this->id));
    }
    
    public function getEmailConfirmed()
    {
        if($this->last_name) return true;
        return FALSE;
    }

    public function getEmail_()
    {
        if($this->social == 'anonim')
            return '';

        return $this->email;
    }
    
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if($this->scenario == 'admin-settings'){
                $pass = $this->password;
                $this->hashPassword($pass);
            }
            return true;
        }
        else
            return false;
    }
    
    public function hashPassword($password)
    {
        $this->password = md5($this->salt . md5($password));
    }
}
