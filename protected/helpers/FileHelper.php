<?php
/* 
 * Работа с файлами и папками
 */

class FileHelper
{
    /**
     * Копирование содержимого директории
     * @param string $from_path Папка, из которой копируют и в конце удалят
     * @param string $to_path Папка, в которую копируют после того, как создадут
     * @return boolean
     */
    public static function copyDirectory($from_path, $to_path)
    {
        if ($from_path != '' && $to_path != '')
        {
            if (!file_exists($to_path)) mkdir($to_path, 0777);
            if (file_exists($from_path) && is_dir($from_path))
            {
                $dir = opendir($from_path);
                while ($file = readdir($dir))
                {
                    if ($file != "." && $file != ".." && $file != ".svn")
                    {
                        if (is_dir($from_path.$file))
                        {
                            FileHelper::copyDirectory($from_path.$file."/", $to_path.$file."/");
                        }
                        if (is_file($from_path.$file))
                        {
                            copy($from_path.$file, $to_path.$file);
                            unlink($from_path.$file);
                        }
                    }
                }
                closedir($dir);
                FileHelper::removeDirectory($from_path);
            }
        }
        return false;
    }

    /**
     * Удаление директории вместе с её содержимым
     * @param string $dir Папка, которую нужно удалить
     * @return boolean
     */
    public static function removeDirectory($dir)
    {
        if (file_exists($dir) && is_dir($dir))
        {
            $opendir = opendir($dir);
            while ($file = readdir($opendir))
            {
                if ($file != "." && $file != "..")
                {
                    /*
                    $handle = fopen("333", "a+");
                    fwrite($handle, mktime().": ".$dir.$file."\n");
                    fclose($handle);*/
                    (file_exists($dir.$file) && is_dir($dir.$file)) ?
                    FileHelper::removeDirectory($dir.$file.'/') : unlink($dir.$file);
                }
            }
            closedir($opendir);
        }
        rmdir($dir);
    }

    /**
     * Создание папки для временного хранения файлов с уникальным именем
     * @param string $file_path Путь к папке, которая объединяет все временные папки
     * @return string $tmp_folder Имя созданной внутренней уникальной временной папки
     * @author Снежана Сторощук
     */
    public static function createTemporaryFolder($file_path='img/overall/temporary/')
    {
        $tmp_folder = mktime();
        while (is_dir($file_path.$tmp_folder."/"))
        {
            $tmp_folder = mktime() + 1;
        }
        mkdir($file_path.$tmp_folder."/");
        return $tmp_folder;
    }

    /**
     * Чтение всех файлов из дериктории
     *
     * @param string $dirName Имя директории
     * @return mixed Массив файлов
     */
    public static function readDir($dirName)
    {
        if ($dirName != null && is_dir($dirName))
        {
            $i = 0;
            $files = array();
            $handle = opendir($dirName);

            // Формируем массив названий файлов с функциями
            while ($file = readdir($handle))
            {
                if ($file != '.' && $file != '..' && $file != '.svn')
                {
                    $files[$i] = $file;    
                    $i ++;
                }
            }
            return $files;
        }
        else
        {
            return false;
        }
    }
    
    public function file_force_download($file) 
    {
        if (file_exists($file)) {
            if (ob_get_level()) {
                ob_end_clean();
            }
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function deleteFile($filename, $path)
    {
        if(FileHelper::fileExists($path.$filename)){
            unlink($path . $filename);
        }
    }

    public function fileExists($path)
    {
        $base = Yii::app()->request->hostInfo;
        $path = $base . '/' . $path;
        $Headers = @get_headers($path);
        if(preg_match("|200|", $Headers[0]))
                return true;
        return false;
    }

    public function getXls($fields, $data, $filename)
    {
        $letters = range('A', 'Z');
        $headerXls = array();
        $setupData = array();
        $width = array();
        foreach($fields as $k => $item){
            $headerXls[$letters[$k]] = $item[0];
            $setupData[$letters[$k]] = $item[1];
            $width[$letters[$k]] = $item[2];
        }
        $new = new Xls();
        $new->setWidth($width);
        $new->setHeader($headerXls);
        $new->setupData($data, $setupData);
//        $path = str_replace('application', '', Yii::app()->basePath). "/../public_html/$filename.xls";

        $path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . "$filename.xls";
        $new->save($path);
        $this->file_force_download($path);
    }
    
    /*
     * Загрузка файла
     */
    
}
