<?php

//// change the following paths if necessary
//$yii=dirname(__FILE__).'/../framework/yii.php';
//$config=dirname(__FILE__).'/protected/config/main.php';
//
//// remove the following lines when in production mode
//defined('YII_DEBUG') or define('YII_DEBUG',true);
//// specify how many levels of call stack should be shown in each log message
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
//
//require_once($yii);
//Yii::createWebApplication($config)->run();

// Set configurations based on environment
if (isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == "development")
{
    // Set framework path
    $yii = dirname(__FILE__) . '/../framework/yii.php';

    // Enable debug mode for development environment
    defined('YII_DEBUG') or define('YII_DEBUG', true);

    // Specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

    // Set environment variable
    $environment = 'development';
}
else
{
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    
    // Set framework path
    $yii = dirname(__FILE__) . '/../framework/yii.php';

    // Set environment variable
    $environment = 'production';
}
date_default_timezone_set('Europe/Kiev');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

// Include config files
$configMain = dirname(__FILE__).'/../protected/config/main.php';

// Include Yii framework
require_once( $yii );

// Run application
$config = $configMain;
Yii::createWebApplication($config)->run();