function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}
function deleteCookie(name) {
    setCookie(name, "", { expires: -1 })
}
function lustration(id){
    var method_id = $('input[name="Lustration[method_id]['+id+']"]:checked').val();
    if(!method_id) {
        $('#method-modal').modal();
        return;
    }
    
    $.ajax({
        type: "POST",
        url: '/candidates/lustra',
        data: {method: method_id, candidate: id},
        success: function(data){
            var obj = $('[candidate='+id+']');
            obj.find('.methods').addClass('lustratiion-result').html(data.html);
            obj.find('.lustruu').remove();
            obj.find('.count').text(data.lustrationsCount);

            // Lets publish result
            if (data.socialType == 'facebook') {
                fbshare(data.url, data.candidateName, data.lustrationsCount, data.punishment);
            } else if (data.socialType == 'vkontakte') {
                vkshare(data.url, data.candidateName, data.lustrationsCount, data.punishment);
            }
        },
        dataType: 'json'
      });
}
function saveLustration(id, set_method_id){
    if (set_method_id) {
        var method_id = set_method_id;
    } else {
        var method_id = $('input[name="Lustration[method_id]['+id+']"]:checked').val();
    }
    if(!method_id) {
        $('#method-modal').modal();
        return;
    }

    setCookie("lustration_candidate", id, {path: "/"});
    setCookie("lustration_method", method_id, {path: "/"});
}
function fbshare(url, candidateName, lustrationsCount, punishment){
    if (typeof FB != 'undefined') {
        FB.ui({
            method: 'feed',
            link: url,
            description: prepareSocialText(lustrationsCount, candidateName, punishment)
        }, function(response){});
    } else {
        setTimeout('fbshare("'+url+'","'+candidateName+'",'+lustrationsCount+',"'+punishment+'")', 1000);
    }
    /*window.open('https://www.facebook.com/sharer/sharer.php?u='+
        url+
        '&p[summary]='+prepareSocialText(lustrationsCount, candidateName, punishment)+
        '&display=popup',
        "",
        "width=600,height=400,status=no,toolbar=no,menubar=no");*/

}
function fbshareSimple(url, description) {
    if (typeof FB != 'undefined') {
        FB.ui({
            method: 'feed',
            link: url,
            description: description
        }, function(response){});
    } else {
        setTimeout('fbshareSimple("'+url+'","'+description+'")', 2000);
    }
}
function vkshare(url, candidateName, lustrationsCount, punishment){
    window.open('http://vk.com/share.php?url='+url+'&description='+prepareSocialText(lustrationsCount, candidateName, punishment)+'&act=share',
        '',
        'width=600,height=400,status=no,toolbar=no,menubar=no');
}
function vkshareSimple(url, description) {
    window.open('http://vk.com/share.php?url='+url+'&description='+description+'&act=share',
        '',
        'width=600,height=400,status=no,toolbar=no,menubar=no');
}
function prepareSocialText(lustrationsCount, candidateName, punishment) {
    text = candidateName + ": люстровано мною";
    if (lustrationsCount - 1 > 0) {
        text += " та ще "+(lustrationsCount - 1)+" людьми";
    }
    text += ". Покарання: "+punishment+"! \nПриєднуйся до Всеукраїнської люстрації!";
    return text;
}
function emailConfirmClose() {
    $.ajax({
        type: "POST",
        url: '/default/index/emailConfirm',
        data: {method: "cancel"},
        success: function(data){
            location.reload();
        },
        dataType: 'json'
    });
}
function anonimAuthorize(){
    $('form[name=anonim-authorize]').submit();
}