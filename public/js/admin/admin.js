$(document).ready(function(){
    $('.add_other_value').each(function(){
        var val = $(this).val();
        var id = $(this).attr('id');
        if(val > 0) $('#'+id+'_other').parents('.control-group:first').hide();
    });

    $('.add_other_value').change(function(){
        var val = $(this).val();
        var id = $(this).attr('id');
        if(val > 0){
            $('#'+id+'_other').parents('.control-group:first').hide();
        }else{
            $('#'+id+'_other').parents('.control-group:first').show();
        }
    });
});

function addNodeDate(obj, node){
    node_cnt = $('input[name=dates_count]').val();
    if(!node_cnt) node_cnt = 1;

    var last_node = $('input.node_date:last').val();
    for(var i = 0; i < node_cnt; i++){
        if(last_node){
            var date = new Date(last_node);
            date.setDate(date.getDate() + 1);
            month = date.getMonth() + 1;
            if(month<10) month = '0'+month;
            day = date.getDate();
            if(day<10) day = '0'+day;
            last_node = date.getFullYear()+'-'+month+'-'+day;
        }
        $(obj).before($('.'+node).html());
        $(obj).prev().find('input').val(last_node);
    }
}

function addChildNode(){
    var number = $('input.children-name').length;
    var html = '<div class="control-group">';
    html += '<label for="Clients_children_names_'+number+'" class="control-label">Имя ребенка</label>';
    html += '<div class="controls">';
    html += '<input class="children-name" type="text" id="Clients_children_names_'+number+'" name="Clients[children_names]['+number+']">';
    html += '</div>';
    html += '</div>';
    html += '<div class="control-group ">';
    html += '<label for="Clients_children_birthdays_'+number+'" class="control-label">День рождения ребенка</label>';
    html += '<div class="controls">';
    html += '<input type="text" id="Clients_children_birthdays_'+number+'" name="Clients[children_birthdays]['+number+']">';
    html += '</div></div>';

    $('.add-node').before(html);
}

function citiesList(obj){
    $.ajax({
        url: "/partners/default/getcities/id/" + $(obj).val(),
        dataType: 'json',
        success: function(data){
            var node = $(obj).parent().parent().next();
            $(node).find('select').html(data.html);
            $(node).find('select').removeAttr('disabled');
            $(node).find('input').attr('disabled', 'disabled');
        },
        error: function(request, status, error) {
            alert(error);
        }
    });
}
