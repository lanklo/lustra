tinyMCE.init({
        theme : "advanced",
//        mode : "exact",
//        elements : "'.$element->getName().'",
        mode : "specific_textareas",
        editor_selector : "mceEditor",
        language : "ru",

        browsers : "msie,safari,gecko,opera",
        font_size_style_values : "8pt,10pt,12pt,14pt,18pt",
        theme_advanced_toolbar_location : "top",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_disable : "fontselect,styleselect,newdocument,indent,outdent,hr,clipboard,sub,sup,visualaid,charmap,anchor,formatselect",
        theme_advanced_buttons1 : "undo,redo,bold,italic,underline,strikethrough,forecolor,backcolor,bullist,numlist,pasteword,pastetext,justifyright,justifycenter,justifyleft,justifyfull,link,unlink,image,media,preview,fontsizeselect,code,removeformat",
        theme_advanced_buttons2 : "tablecontrols",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_buttons5 : "",
        theme_advanced_blockformats : "p,div,h1,h2,h3,h4,h5,h6,blockquote,dt,dd,code,samp",
        plugins : "contextmenu,fullscreen,paste,advlink,advimage,preview,media,table",
        extended_valid_elements : "hr[class|width|size|noshade],iframe[frameborder|src|width|height|name|align]",
        file_browser_callback : "ajaxfilemanager",
        paste_use_dialog : false,
        theme_advanced_resizing : true,
        theme_advanced_resize_horizontal : true,
        apply_source_formatting : true,
        force_br_newlines : true,
        force_p_newlines : false,
        relative_urls : false,
        table_inline_editing : true

});

function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "/js/admin/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    switch (type) {
        case "image":
            break;
        case "file":
            break;
        default:
            return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: "/js/admin/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
        width: 782,
        height: 440,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });
}